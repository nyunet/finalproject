using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest
{
    public class UserService_SearchPostsByFilters
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                   .UseInMemoryDatabase(databaseName: "test")
                   .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void SearchPostsByFilters()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 2);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 2);

                var service = new UserService(context);
                posts = service.SearchPostsByFilters();
            }
            Assert.Equal(3, posts.Count);
            foreach (Post p in posts)
            {
                Assert.NotNull(p);
                Assert.Equal("b", p.OwnerId);
            }
        }

        [Fact]
        public void FilterPosts_CategoryFilter()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 2);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 2);

                var service = new UserService(context);
                posts = service.SearchPostsByFilters(categoryId: 1);
            }
            Assert.Equal(3, posts.Count);
            foreach (Post p in posts)
            {
                Assert.NotNull(p);
                Assert.Equal("b", p.OwnerId);
                Assert.Equal(1, p.CategoryId);
            }
        }

        [Fact]
        public void FilterPosts_CategorySubcategoryFilter()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 2);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 2);

                var service = new UserService(context);
                posts = service.SearchPostsByFilters(categoryId: 1, subcategoryId: 1);
            }
            Assert.Single(posts);
            foreach (Post p in posts)
            {
                Assert.NotNull(p);
                Assert.Equal("b", p.OwnerId);
                Assert.Equal(1, p.CategoryId);
                Assert.Equal(1, p.SubcategoryId);
            }
        }

        [Fact]
        public void FilterPosts_AreaFilter()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 2);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 2);

                var service = new UserService(context);
                posts = service.SearchPostsByFilters(areaId: 1);
            }
            Assert.Equal(3, posts.Count);
            foreach (Post p in posts)
            {
                Assert.NotNull(p);
                Assert.Equal("b", p.OwnerId);
                Assert.Equal(1, p.AreaId);
            }
        }

        [Fact]
        public void FilterPosts_AreaLocaleFilter()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 2);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 2);

                var service = new UserService(context);
                posts = service.SearchPostsByFilters(areaId: 1, localeId: 2);
            }
            Assert.Single(posts);
            foreach (Post p in posts)
            {
                Assert.NotNull(p);
                Assert.Equal("b", p.OwnerId);
                Assert.Equal(1, p.AreaId);
                Assert.Equal(2, p.LocaleId);
            }
        }


        [Fact]
        public void FilterPosts_LocaleIdFilterOnlyIsIgnored()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 2);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 2);

                var service = new UserService(context);
                posts = service.SearchPostsByFilters(localeId: 1);
            }
            Assert.Equal(3, posts.Count);
            foreach (Post p in posts)
            {
                Assert.NotNull(p);
                Assert.Equal("b", p.OwnerId);
            }
        }

        [Fact]
        public void FilterPosts_SubcategoryIdFilterOnlyIsIgnored()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 2);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 2);

                var service = new UserService(context);
                posts = service.SearchPostsByFilters(subcategoryId: 1);
            }
            Assert.Equal(3, posts.Count);
            foreach (Post p in posts)
            {
                Assert.NotNull(p);
                Assert.Equal("b", p.OwnerId);
            }
        }

        [Fact]
        public void FilterPosts_SubcategoryIdLocalIdFiltersOnlyAreIgnored()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 2);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 2);

                var service = new UserService(context);
                posts = service.SearchPostsByFilters(subcategoryId: 1, localeId: 1);
            }
            Assert.Equal(3, posts.Count);
            foreach (Post p in posts)
            {
                Assert.NotNull(p);
                Assert.Equal("b", p.OwnerId);
            }
        }
    }
}
