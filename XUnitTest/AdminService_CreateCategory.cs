﻿using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest
{
    public class AdminService_CreateCategory
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                    .UseInMemoryDatabase(databaseName: "test")
                    .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void CreateCategory_FailAdminInvalid()
        {
            Outcome outcome;
            List<Category> categories;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                var service = new AdminService(context);
                outcome = service.CreateCategory("c", "NewCategory");
                categories = context.Categories.ToList();
            }
            Assert.Equal("User ID does not correspond to an administrator", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Empty(categories);
        }

        [Fact]
        public void CreateCategory_FailCategoryExists()
        {
            Outcome outcome;
            List<Category> categories;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                var service = new AdminService(context);
                outcome = service.CreateCategory("b", "Electronics");
                categories = context.Categories.ToList();
            }
            Assert.Equal("Category name already exists", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Single(categories);
        }


        [Fact]
        public void CreateCategory_Success()
        {
            Outcome outcome;
            List<Category> categories;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                var service = new AdminService(context);
                outcome = service.CreateCategory("b", "Electronics");
                categories = context.Categories.ToList();
            }
            Assert.Equal("", outcome.Message());
            Assert.False(outcome.fail);
            Assert.Single(categories);
            Assert.Equal("Electronics", categories[0].Name);
            Assert.False(categories[0].Hidden);
        }
    }
}
