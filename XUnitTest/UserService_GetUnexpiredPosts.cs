using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest
{
    public class UserService_GetUnexpiredPosts
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                   .UseInMemoryDatabase(databaseName: "test")
                   .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void GetUnexpiredPosts_Allofb_JustCreated()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 2, 1, 2);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 1);
                helper.CreatePost(context, "Title4", "Body4", "c", 1, 2, 1, 1);

                var service = new UserService(context);
                posts = service.GetUnexpiredPosts("b", DateTime.Now);
            }
            Assert.Equal(3, posts.Count);
            foreach (Post p in posts)
            {
                Assert.NotNull(p);
                Assert.Equal("b", p.OwnerId);
            }
        }

        [Fact]
        public void GetUnexpiredPosts_Allofb_CreatedThreeDaysAgo()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 2, 1, 2);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 1);
                helper.CreatePost(context, "Title4", "Body4", "c", 1, 2, 1, 1);

                var service = new UserService(context);
                posts = service.GetUnexpiredPosts("b", DateTime.Now.AddDays(3));
            }
            Assert.Equal(3, posts.Count);
            foreach (Post p in posts)
            {
                Assert.NotNull(p);
                Assert.Equal("b", p.OwnerId);
            }
        }

        [Fact]
        public void GetUnexpiredPosts_Noneofb_CreatedSixDaysAgo()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 2, 1, 2);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 1);
                helper.CreatePost(context, "Title4", "Body4", "c", 1, 2, 1, 1);

                var service = new UserService(context);
                posts = service.GetUnexpiredPosts("b", DateTime.Now.AddDays(6));
            }
            Assert.Empty(posts);
        }

        [Fact]
        public void GetUnexpiredPosts_Allofc_CreatedThreeDaysAgo()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 2, 1, 2);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 1);
                helper.CreatePost(context, "Title4", "Body4", "c", 1, 2, 1, 1);

                var service = new UserService(context);
                posts = service.GetUnexpiredPosts("c", DateTime.Now.AddDays(3));
            }
            Assert.Single(posts);
            Post p = posts[0];
            Assert.NotNull(p);
            Assert.Equal("Title4", p.Title);
            Assert.Equal("Body4", p.Body);
            Assert.Equal("c", p.OwnerId);
            Assert.Equal(1, p.AreaId);
            Assert.Equal(2, p.LocaleId);
            Assert.Equal(1, p.CategoryId);
            Assert.Equal(1, p.SubcategoryId);
        }

        [Fact]
        public void GetUnexpiredPosts_Noneofc_CreatedSixDaysAgo()
        {
            List<Post> posts;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 2, 1, 2);
                helper.CreatePost(context, "Title2", "Body2", "b", 1, 1, 1, 1);
                helper.CreatePost(context, "Title3", "Body3", "b", 1, 2, 1, 1);
                helper.CreatePost(context, "Title4", "Body4", "c", 1, 2, 1, 1);

                var service = new UserService(context);
                posts = service.GetUnexpiredPosts("c", DateTime.Now.AddDays(6));
            }
            Assert.Empty(posts);
        }
    }
}
