﻿using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using Xunit;

namespace XUnitTest
{
    public class AdminService_IsAdminValid
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                    .UseInMemoryDatabase(databaseName: "test")
                    .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void IsAdminValid_FalseNonExistant()
        {
            bool valid;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                var service = new AdminService(context);
                valid = service.IsAdminValid("c");
            }
            Assert.False(valid);
        }

        [Fact]
        public void IsAdminValid_FalseUser()
        {
            bool valid;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                var service = new AdminService(context);
                valid = service.IsAdminValid("c");
            }
            Assert.False(valid);
        }

        [Fact]
        public void IsAdminValid_True()
        {
            bool valid;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                var service = new AdminService(context);
                valid = service.IsAdminValid("b");
            }
            Assert.True(valid);
        }
    }
}
