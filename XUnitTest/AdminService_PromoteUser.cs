﻿using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using Xunit;

namespace XUnitTest
{
    public class AdminService_PromoteUser
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                    .UseInMemoryDatabase(databaseName: "test")
                    .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void PromoteUser_FailAdminInvalid()
        {
            Outcome outcome;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                var service = new AdminService(context);
                outcome = service.PromoteUser("c", "c");
            }
            Assert.Equal("User ID does not correspond to an administrator", outcome.Message());
            Assert.True(outcome.fail);
        }

        [Fact]
        public void PromoteUser_FailPromotingAdmin()
        {
            Outcome outcome;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                var service = new AdminService(context);
                outcome = service.PromoteUser("b", "b");
            }
            Assert.Equal("User ID does not correspond to a user", outcome.Message());
            Assert.True(outcome.fail);
        }

        [Fact]
        public void PromoteUser_Success()
        {
            Outcome outcome;
            User u;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                var service = new AdminService(context);
                outcome = service.PromoteUser("b", "c");
                u = context.Users.Find("c");
            }
            Assert.Equal("", outcome.Message());
            Assert.False(outcome.fail);
            Assert.Equal("c", u.Id);
            Assert.Equal("admin", u.Type);
            Assert.Equal("Carol", u.Name);
        }
    }
}
