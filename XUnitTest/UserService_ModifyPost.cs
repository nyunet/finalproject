using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest
{
    public class UserService_ModifyPost
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                   .UseInMemoryDatabase(databaseName: "test")
                   .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void ModifyPost_SuccessFull()
        {
            Post p;
            Outcome outcome;

            int PostId = 1;
            string Title = "New title";
            string Body = "The new body";
            string OwnerId = "b";
            int AreaId = 2, LocaleId = 1;
            int CategoryId = 2, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateArea(context, "Brooklyn", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreatePopulateCategory(context, "Gardening", new List<string> { "Machines", "Tools" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Original title", "Original body", "b", 1, 3, 1, 1);

                var service = new UserService(context);
                outcome = service.ModifyPost(PostId, Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("", outcome.Message());
            Assert.False(outcome.fail);
            Assert.NotNull(p);
            Assert.Equal("New title", p.Title);
            Assert.Equal("The new body", p.Body);
            Assert.Equal("b", p.OwnerId);
            Assert.Equal(2, p.AreaId);
            Assert.Equal(1, p.LocaleId);
            Assert.Equal(2, p.CategoryId);
            Assert.Equal(2, p.SubcategoryId);
        }

        [Fact]
        public void ModifyPost_SuccessPartial()
        {
            Post p;
            Outcome outcome;

            int PostId = 1;
            string Title = "New title";
            string Body = "The new body";
            int CategoryId = 2, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateArea(context, "Brooklyn", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreatePopulateCategory(context, "Gardening", new List<string> { "Machines", "Tools" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Original title", "Original body", "b", 1, 3, 1, 1);

                var service = new UserService(context);
                outcome = service.ModifyPost(PostId, Title: Title, Body: Body, CategoryId: CategoryId, SubcategoryId: SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("", outcome.Message());
            Assert.False(outcome.fail);
            Assert.NotNull(p);
            Assert.Equal("New title", p.Title);
            Assert.Equal("The new body", p.Body);
            Assert.Equal("b", p.OwnerId);
            Assert.Equal(1, p.AreaId);
            Assert.Equal(3, p.LocaleId);
            Assert.Equal(2, p.CategoryId);
            Assert.Equal(2, p.SubcategoryId);
        }

        [Fact]
        public void ModifyPost_FailNonExistantPostId()
        {
            Post p;
            Outcome outcome;

            int PostId = 2;
            string Title = "New title";
            string Body = "The new body";
            string OwnerId = "b";
            int AreaId = 2, LocaleId = 1;
            int CategoryId = 2, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateArea(context, "Brooklyn", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreatePopulateCategory(context, "Gardening", new List<string> { "Machines", "Tools" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Original title", "Original body", "b", 1, 3, 1, 1);

                var service = new UserService(context);
                outcome = service.ModifyPost(PostId, Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("Post ID does not correspond to a post", outcome.Message());
            Assert.True(outcome.fail);
            Assert.NotNull(p);
            Assert.Equal("Original title", p.Title);
            Assert.Equal("Original body", p.Body);
            Assert.Equal("b", p.OwnerId);
            Assert.Equal(1, p.AreaId);
            Assert.Equal(3, p.LocaleId);
            Assert.Equal(1, p.CategoryId);
            Assert.Equal(1, p.SubcategoryId);
        }

        [Fact]
        public void ModifyPost_FailNonExistantOwnerId()
        {
            Post p;
            Outcome outcome;

            int PostId = 1;
            string Title = "New title";
            string Body = "The new body";
            string OwnerId = "c";
            int AreaId = 2, LocaleId = 1;
            int CategoryId = 2, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateArea(context, "Brooklyn", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreatePopulateCategory(context, "Gardening", new List<string> { "Machines", "Tools" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Original title", "Original body", "b", 1, 3, 1, 1);

                var service = new UserService(context);
                outcome = service.ModifyPost(PostId, Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("Post owner ID does not correspond to a user", outcome.Message());
            Assert.True(outcome.fail);
            Assert.NotNull(p);
            Assert.Equal("Original title", p.Title);
            Assert.Equal("Original body", p.Body);
            Assert.Equal("b", p.OwnerId);
            Assert.Equal(1, p.AreaId);
            Assert.Equal(3, p.LocaleId);
            Assert.Equal(1, p.CategoryId);
            Assert.Equal(1, p.SubcategoryId);
        }

        [Fact]
        public void ModifyPost_FailOwnerIdIsAdmin()
        {
            Post p;
            Outcome outcome;

            int PostId = 1;
            string Title = "New title";
            string Body = "The new body";
            string OwnerId = "c";
            int AreaId = 2, LocaleId = 1;
            int CategoryId = 2, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateArea(context, "Brooklyn", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreatePopulateCategory(context, "Gardening", new List<string> { "Machines", "Tools" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreateUser(context, "c", "admin", "John");
                helper.CreatePost(context, "Original title", "Original body", "b", 1, 3, 1, 1);

                var service = new UserService(context);
                outcome = service.ModifyPost(PostId, Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("Post owner ID does not correspond to a user", outcome.Message());
            Assert.True(outcome.fail);
            Assert.NotNull(p);
            Assert.Equal("Original title", p.Title);
            Assert.Equal("Original body", p.Body);
            Assert.Equal("b", p.OwnerId);
            Assert.Equal(1, p.AreaId);
            Assert.Equal(3, p.LocaleId);
            Assert.Equal(1, p.CategoryId);
            Assert.Equal(1, p.SubcategoryId);
        }

        [Fact]
        public void ModifyPost_FailNonExistantAreaId()
        {
            Post p;
            Outcome outcome;

            int PostId = 1;
            string Title = "New title";
            string Body = "The new body";
            string OwnerId = "b";
            int AreaId = 5, LocaleId = 1;
            int CategoryId = 2, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateArea(context, "Brooklyn", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreatePopulateCategory(context, "Gardening", new List<string> { "Machines", "Tools" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Original title", "Original body", "b", 1, 3, 1, 1);

                var service = new UserService(context);
                outcome = service.ModifyPost(PostId, Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("Area ID does not correspond to an area", outcome.Message());
            Assert.True(outcome.fail);
            Assert.NotNull(p);
            Assert.Equal("Original title", p.Title);
            Assert.Equal("Original body", p.Body);
            Assert.Equal("b", p.OwnerId);
            Assert.Equal(1, p.AreaId);
            Assert.Equal(3, p.LocaleId);
            Assert.Equal(1, p.CategoryId);
            Assert.Equal(1, p.SubcategoryId);
        }

        [Fact]
        public void ModifyPost_FailNonExistantLocaleId()
        {
            Post p;
            Outcome outcome;

            int PostId = 1;
            string Title = "New title";
            string Body = "The new body";
            string OwnerId = "b";
            int AreaId = 2, LocaleId = 10;
            int CategoryId = 2, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateArea(context, "Brooklyn", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreatePopulateCategory(context, "Gardening", new List<string> { "Machines", "Tools" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Original title", "Original body", "b", 1, 3, 1, 1);

                var service = new UserService(context);
                outcome = service.ModifyPost(PostId, Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("Locale ID does not correspond to a locale", outcome.Message());
            Assert.True(outcome.fail);
            Assert.NotNull(p);
            Assert.Equal("Original title", p.Title);
            Assert.Equal("Original body", p.Body);
            Assert.Equal("b", p.OwnerId);
            Assert.Equal(1, p.AreaId);
            Assert.Equal(3, p.LocaleId);
            Assert.Equal(1, p.CategoryId);
            Assert.Equal(1, p.SubcategoryId);
        }

        [Fact]
        public void ModifyPost_FailNonExistantCategoryId()
        {
            Post p;
            Outcome outcome;

            int PostId = 1;
            string Title = "New title";
            string Body = "The new body";
            string OwnerId = "b";
            int AreaId = 2, LocaleId = 2;
            int CategoryId = 5, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateArea(context, "Brooklyn", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreatePopulateCategory(context, "Gardening", new List<string> { "Machines", "Tools" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Original title", "Original body", "b", 1, 3, 1, 1);

                var service = new UserService(context);
                outcome = service.ModifyPost(PostId, Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("Category ID does not correspond to a category", outcome.Message());
            Assert.True(outcome.fail);
            Assert.NotNull(p);
            Assert.Equal("Original title", p.Title);
            Assert.Equal("Original body", p.Body);
            Assert.Equal("b", p.OwnerId);
            Assert.Equal(1, p.AreaId);
            Assert.Equal(3, p.LocaleId);
            Assert.Equal(1, p.CategoryId);
            Assert.Equal(1, p.SubcategoryId);
        }

        [Fact]
        public void ModifyPost_FailNonExistantSubcategoryId()
        {
            Post p;
            Outcome outcome;

            int PostId = 1;
            string Title = "New title";
            string Body = "The new body";
            string OwnerId = "b";
            int AreaId = 2, LocaleId = 2;
            int CategoryId = 2, SubcategoryId = 10;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateArea(context, "Brooklyn", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreatePopulateCategory(context, "Gardening", new List<string> { "Machines", "Tools" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Original title", "Original body", "b", 1, 3, 1, 1);

                var service = new UserService(context);
                outcome = service.ModifyPost(PostId, Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("Subcategory ID does not correspond to a subcategory", outcome.Message());
            Assert.True(outcome.fail);
            Assert.NotNull(p);
            Assert.Equal("Original title", p.Title);
            Assert.Equal("Original body", p.Body);
            Assert.Equal("b", p.OwnerId);
            Assert.Equal(1, p.AreaId);
            Assert.Equal(3, p.LocaleId);
            Assert.Equal(1, p.CategoryId);
            Assert.Equal(1, p.SubcategoryId);
        }
    }
}
