﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XUnitTest
{
    class BusinessLogicTestHelper
    {
        public void CreatePopulateArea(BusinessDbContext context, string AreaName, List<string> LocaleNames)
        {
            Area a = new Area { Name = AreaName };
            context.Areas.Add(a);
            context.SaveChanges();
            int AreaId = a.Id;
            foreach (string Name in LocaleNames)
            {
                Locale l = new Locale { Name = Name, AreaId = AreaId };
                context.Locales.Add(l);
            }
            context.SaveChanges();
        }

        public void CreatePopulateCategory(BusinessDbContext context, string CategoryName, List<string> SubcategoryNames)
        {
            Category c = new Category { Name = CategoryName };
            context.Categories.Add(c);
            context.SaveChanges();
            int CategoryId = c.Id;
            foreach (string Name in SubcategoryNames)
            {
                Subcategory sc = new Subcategory { Name = Name, CategoryId = CategoryId };
                context.Subcategories.Add(sc);
            }
            context.SaveChanges();
        }

        public void CreateUser(BusinessDbContext context, string Id, string Type, string Name)
        {
            User u = new User { Id = Id, Name = Name, Type = Type };
            context.Users.Add(u);
            context.SaveChanges();
        }

        public void CreatePost(BusinessDbContext context, string Title, string Body, string OwnerId,
            int AreaId, int LocaleId, int CategoryId, int SubcategoryId)
        {
            // Only checks for userId, not for categories and so on...
            if (context.Users.Find(OwnerId) != null)
            {
                Post p = new Post
                {
                    Title = Title,
                    Body = Body,
                    OwnerId = OwnerId,
                    AreaId = AreaId,
                    LocaleId = LocaleId,
                    CategoryId = CategoryId,
                    SubcategoryId = SubcategoryId
                };
                context.Posts.Add(p);
                context.SaveChanges();
                return;
            }
            // TODO error
        }

        public void CreateMessage(BusinessDbContext context, string Response, string SenderId, string ReceiverId, int PostId)
        {
            Post post = context.Posts.Find(PostId);
            if (post != null)
            {
                Message m = new Message
                {
                    Response = Response,
                    SenderId = SenderId,
                    ReceiverId = ReceiverId,
                    PostId = PostId
                };
                context.Messages.Add(m);
                context.SaveChanges();
            }
            // TODO error
        }
    }
}
