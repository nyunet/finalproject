using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest
{
    public class UserService_CreateMessage
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                   .UseInMemoryDatabase(databaseName: "test")
                   .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void CreateMessage_Success()
        {
            Message m;
            Outcome outcome;

            int postId = 1;
            string SenderId = "b";
            string ReceiverId = "c";
            string Response = "My message is beautiful";
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePost(context, "Title1", "Body1", "c", 1, 1, 1, 1);

                var service = new UserService(context);
                outcome = service.CreateMessage(postId, SenderId, ReceiverId, Response);
                m = context.Messages.SingleOrDefault();
            }
            Assert.Equal("", outcome.Message());
            Assert.False(outcome.fail);
            Assert.NotNull(m);
            Assert.Equal(1, m.PostId);
            Assert.Equal("b", m.SenderId);
            Assert.Equal("My message is beautiful", m.Response);
        }

        // TODO make annotations work

        [Fact]
        public void CreateMessage_Fail_NoUserId()
        {
            Message m;
            Outcome outcome;

            int postId = 1;
            string SenderId = "nonexistant";
            string ReceiverId = "c";
            string Response = "My message is beautiful";
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePost(context, "Title1", "Body1", "c", 1, 1, 1, 1);

                var service = new UserService(context);
                outcome = service.CreateMessage(postId, SenderId, ReceiverId, Response);
                m = context.Messages.SingleOrDefault();
            }
            Assert.Equal("User ID does not correspond to a user", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Null(m);
        }

        [Fact]
        public void CreateMessage_Fail_NoPostId()
        {
            Message m;
            Outcome outcome;

            int postId = 3;
            string SenderId = "b";
            string recieverId = "c";
            string Response = "My message is beautiful";
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePost(context, "Title1", "Body1", "c", 1, 1, 1, 1);

                var service = new UserService(context);
                outcome = service.CreateMessage(postId, SenderId, recieverId, Response);
                m = context.Messages.SingleOrDefault();
            }
            Assert.Equal("Post ID does not correspond to a post", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Null(m);
        }
    }
}
