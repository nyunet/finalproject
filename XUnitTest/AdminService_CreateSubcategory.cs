﻿using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest
{
    public class AdminService_CreateSubcategory
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                    .UseInMemoryDatabase(databaseName: "test")
                    .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void CreateSubcategory_FailAdminInvalid()
        {
            Outcome outcome;
            List<Subcategory> subcategories;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                var service = new AdminService(context);
                outcome = service.CreateSubcategory("c", "NewSubcategory", 1);
                subcategories = context.Subcategories.ToList();
            }
            Assert.Equal("User ID does not correspond to an administrator", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Equal(2, subcategories.Count);
        }

        [Fact]
        public void CreateSubcategory_FailCategoryNonExistant()
        {
            Outcome outcome;
            List<Subcategory> subcategories;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                var service = new AdminService(context);
                outcome = service.CreateSubcategory("b", "Desktops", 2);
                subcategories = context.Subcategories.ToList();
            }
            Assert.Equal("Category ID does not correspond to an existing category", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Equal(2, subcategories.Count);
        }

        [Fact]
        public void CreateSubcategory_FailSubcategoryExistsForCategory()
        {
            Outcome outcome;
            List<Subcategory> subcategories;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                var service = new AdminService(context);
                outcome = service.CreateSubcategory("b", "Desktops", 1);
                subcategories = context.Subcategories.ToList();
            }
            Assert.Equal("Subcategory name already exists for this category", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Equal(2, subcategories.Count);
        }

        [Fact]
        public void CreateSubcategory_Success()
        {
            Outcome outcome;
            List<Subcategory> subcategories;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                var service = new AdminService(context);
                outcome = service.CreateSubcategory("b", "Phones", 1);
                subcategories = context.Subcategories.ToList();
            }
            Assert.Equal("", outcome.Message());
            Assert.False(outcome.fail);
            Assert.Equal(3, subcategories.Count);
            Assert.Equal("Phones", subcategories[2].Name);
        }
    }
}
