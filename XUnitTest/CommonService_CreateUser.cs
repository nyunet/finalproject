﻿using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using Xunit;

namespace XUnitTest
{
    public class CommonService_CreateUser
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                    .UseInMemoryDatabase(databaseName: "test")
                    .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void CreateUser_Admin()
        {
            User u;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                var service = new CommonService(context);
                service.CreateUser("b", "John");

                u = context.Users.Find("b");
            }
            Assert.NotNull(u);
            Assert.Equal("b", u.Id);
            Assert.Equal("John", u.Name);
            Assert.Equal("admin", u.Type);
        }

        [Fact]
        public void CreateUser_User()
        {
            User u;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "c", "user", "Carol");

                var service = new CommonService(context);
                service.CreateUser("b", "John");

                u = context.Users.Find("b");
            }
            Assert.NotNull(u);
            Assert.Equal("b", u.Id);
            Assert.Equal("John", u.Name);
            Assert.Equal("user", u.Type);
        }
    }
}
