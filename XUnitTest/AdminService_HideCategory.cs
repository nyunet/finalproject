﻿using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest
{
    public class AdminService_HideCategory
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                    .UseInMemoryDatabase(databaseName: "test")
                    .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void HideCategory_FailAdminInvalid()
        {
            Outcome outcome;
            List<Category> categories;
            List<Subcategory> subcategories;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                var service = new AdminService(context);
                outcome = service.HideCategory("c", 1);
                categories = context.Categories.ToList();
                subcategories = context.Subcategories.ToList();
            }
            Assert.Equal("User ID does not correspond to an administrator", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Single(categories);
            Assert.Equal("Electronics", categories[0].Name);
            Assert.False(categories[0].Hidden);
            Assert.Equal(1, categories[0].Id);
            Assert.Equal(2, subcategories.Count);
            Assert.Equal("Desktops", subcategories[0].Name);
            Assert.Equal(1, subcategories[0].CategoryId);
            Assert.False(subcategories[0].Hidden);
            Assert.Equal("Laptops", subcategories[1].Name);
            Assert.Equal(1, subcategories[1].CategoryId);
            Assert.False(subcategories[1].Hidden);
        }

        [Fact]
        public void HideCategory_FailCategoryNonExistant()
        {
            Outcome outcome;
            List<Category> categories;
            List<Subcategory> subcategories;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                var service = new AdminService(context);
                outcome = service.HideCategory("b", 2);
                categories = context.Categories.ToList();
                subcategories = context.Subcategories.ToList();
            }
            Assert.Equal("Category ID does not correspond to an existing category", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Single(categories);
            Assert.Equal("Electronics", categories[0].Name);
            Assert.False(categories[0].Hidden);
            Assert.Equal(1, categories[0].Id);
            Assert.Equal(2, subcategories.Count);
            Assert.Equal("Desktops", subcategories[0].Name);
            Assert.Equal(1, subcategories[0].CategoryId);
            Assert.False(subcategories[0].Hidden);
            Assert.Equal("Laptops", subcategories[1].Name);
            Assert.Equal(1, subcategories[1].CategoryId);
            Assert.False(subcategories[1].Hidden);
        }


        [Fact]
        public void HideCategory_Success()
        {
            Outcome outcome;
            List<Category> categories;
            List<Subcategory> subcategories;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreateUser(context, "b", "admin", "John");
                helper.CreateUser(context, "c", "user", "Carol");
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                var service = new AdminService(context);
                outcome = service.HideCategory("b", 1);
                categories = context.Categories.ToList();
                subcategories = context.Subcategories.ToList();
            }
            Assert.Equal("", outcome.Message());
            Assert.False(outcome.fail);
            Assert.Single(categories);
            Assert.Equal("Electronics", categories[0].Name);
            Assert.True(categories[0].Hidden);
            Assert.Equal(1, categories[0].Id);
            Assert.Equal(2, subcategories.Count);
            Assert.Equal("Desktops", subcategories[0].Name);
            Assert.Equal(1, subcategories[0].CategoryId);
            Assert.True(subcategories[0].Hidden);
            Assert.Equal("Laptops", subcategories[1].Name);
            Assert.Equal(1, subcategories[1].CategoryId);
            Assert.True(subcategories[1].Hidden);
        }
    }
}
