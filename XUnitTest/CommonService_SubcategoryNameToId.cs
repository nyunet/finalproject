﻿using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using Xunit;

namespace XUnitTest
{
    public class CommonService_SubcategoryNameToId
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                    .UseInMemoryDatabase(databaseName: "test")
                    .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void SubcategoryNameToId_Nonexistant()
        {
            int subcategoryId;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                var service = new CommonService(context);
                subcategoryId = service.SubcategoryNameToId("Glue");
            }
            Assert.Equal(-1, subcategoryId);
        }

        [Fact]
        public void SubcategoryNameToId_Existant()
        {
            int subcategoryId;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                var service = new CommonService(context);
                subcategoryId = service.SubcategoryNameToId("Laptops");
            }
            Assert.Equal(2, subcategoryId);
        }
    }
}
