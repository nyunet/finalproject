using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using Xunit;

namespace XUnitTest
{
    public class UserService_SearchByKeywords
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                   .UseInMemoryDatabase(databaseName: "test")
                   .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void EmptyQueryAll()
        {
            List<Post> results;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Computers" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context,
                    "Dell Laptop E2100", "It's a great gaming laptop. It has Intel inside",
                    "b", 1, 1, 1, 1);
                helper.CreatePost(context,
                    "Dell Desktop 2009", "It has a gaming graphics card.",
                    "b", 1, 1, 1, 1);
                var service = new UserService(context);
                results = service.SearchPostsByKeywords("");
            }
            Assert.Equal(2, results.Count);
        }

        [Fact]
        public void SpacesOnlyAll()
        {
            List<Post> results;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Computers" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context,
                    "Dell Laptop E2100", "It's a great gaming laptop. It has Intel inside",
                    "b", 1, 1, 1, 1);
                helper.CreatePost(context,
                    "Dell Desktop 2009", "It has a gaming graphics card.",
                    "b", 1, 1, 1, 1);
                var service = new UserService(context);
                results = service.SearchPostsByKeywords("   ");
            }
            Assert.Equal(2, results.Count);
        }

        [Fact]
        public void SingleWordIrrelevantNoResult()
        {
            List<Post> results;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Computers" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context,
                    "Dell Laptop E2100", "It's a great gaming laptop. It has Intel inside",
                    "b", 1, 1, 1, 1);
                helper.CreatePost(context,
                    "Dell Desktop 2009", "It has a gaming graphics card.",
                    "b", 1, 1, 1, 1);
                var service = new UserService(context);
                results = service.SearchPostsByKeywords("nonexistant");
            }
            Assert.Empty(results);
        }


        [Fact]
        public void SingleWordSingleMatchInTitle()
        {
            List<Post> results;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Computers" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context,
                    "Dell Laptop E2100", "It's a great gaming laptop. It has Intel inside",
                    "b", 1, 1, 1, 1);
                helper.CreatePost(context,
                    "Dell Desktop 2009", "It has a gaming graphics card.",
                    "b", 1, 1, 1, 1);
                var service = new UserService(context);
                results = service.SearchPostsByKeywords("E2100");
            }
            Assert.Single(results);
            Post p = results[0];
            Assert.Equal("Dell Laptop E2100", p.Title);
        }

        [Fact]
        public void SingleWordSingleMatchInBody()
        {
            List<Post> results;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Computers" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context,
                    "Dell Laptop E2100", "It's a great gaming laptop. It has Intel inside",
                    "b", 1, 1, 1, 1);
                helper.CreatePost(context,
                    "Dell Desktop 2009", "It has a gaming graphics card.",
                    "b", 1, 1, 1, 1);
                var service = new UserService(context);
                results = service.SearchPostsByKeywords("Intel");
            }
            Assert.Single(results);
            Post p = results[0];
            Assert.Equal("Dell Laptop E2100", p.Title);
        }

        [Fact]
        public void SingleWordMultipleMatchesInTitle()
        {
            List<Post> results;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Computers" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context,
                    "Dell Laptop E2100", "It's a great gaming laptop. It has Intel inside",
                    "b", 1, 1, 1, 1);
                helper.CreatePost(context,
                    "Dell Desktop 2009", "It has a gaming graphics card.",
                    "b", 1, 1, 1, 1);
                var service = new UserService(context);
                results = service.SearchPostsByKeywords("Dell");
            }
            Assert.Equal(2, results.Count);
        }

        [Fact]
        public void SingleWordMultipleMatchesInBody()
        {
            List<Post> results;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Computers" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context,
                    "Dell Laptop E2100", "It's a great gaming laptop. It has Intel inside",
                    "b", 1, 1, 1, 1);
                helper.CreatePost(context,
                    "Dell Desktop 2009", "It has a gaming graphics card.",
                    "b", 1, 1, 1, 1);
                var service = new UserService(context);
                results = service.SearchPostsByKeywords("gaming");
            }
            Assert.Equal(2, results.Count);
        }

        [Fact]
        public void MultipleWordsIrrelevantNoMatch()
        {
            List<Post> results;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Computers" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context,
                    "Dell Laptop E2100", "It's a great gaming laptop. It has Intel inside",
                    "b", 1, 1, 1, 1);
                helper.CreatePost(context,
                    "Dell Desktop 2009", "It has a gaming graphics card.",
                    "b", 1, 1, 1, 1);
                var service = new UserService(context);
                results = service.SearchPostsByKeywords("bla tester");
            }
            Assert.Empty(results);
        }

        [Fact]
        public void MultipleWordsConflictingNoMatch()
        {
            List<Post> results;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Computers" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context,
                    "Dell Laptop E2100", "It's a great gaming laptop. It has Intel inside",
                    "b", 1, 1, 1, 1);
                helper.CreatePost(context,
                    "Dell Desktop 2009", "It has a gaming graphics card.",
                    "b", 1, 1, 1, 1);
                var service = new UserService(context);
                results = service.SearchPostsByKeywords("laptop desktop");
            }
            Assert.Empty(results);
        }

        [Fact]
        public void MultipleWordsSingleMatch()
        {
            List<Post> results;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Computers" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context,
                    "Dell Laptop E2100", "It's a great gaming laptop. It has Intel inside",
                    "b", 1, 1, 1, 1);
                helper.CreatePost(context,
                    "Dell Desktop 2009", "It has a gaming graphics card.",
                    "b", 1, 1, 1, 1);
                var service = new UserService(context);
                results = service.SearchPostsByKeywords("Dell Intel");
            }

            Assert.Single(results);
        }
    }
}
