﻿using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using Xunit;
using System.Linq;
using System.Diagnostics;
using System;
using Xunit.Abstractions;

namespace XUnitTest
{
    public class CommonService_ListFunctions
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();
        private readonly ITestOutputHelper output;


        public CommonService_ListFunctions(ITestOutputHelper output)
        {
            this.output = output;
        }

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                    .UseInMemoryDatabase(databaseName: "test")
                    .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void ListAllById()
        {
            List<Area> areas;
            List<Locale> locales;
            List<Category> categories;
            List<Subcategory> subcategories;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                var service = new CommonService(context);
                areas = service.ListAreas();
                locales = service.ListLocales(1);
                categories = service.ListCategories();
                subcategories = service.ListSubcategories(1);
            }
            Assert.Single(areas);
            Area a = areas[0];
            Assert.Equal("NYC", a.Name);
            Assert.Equal(3, locales.Count);
            Assert.Single(categories);
            Category c = categories[0];
            Assert.Equal("Electronics", c.Name);
            Assert.Equal(2, subcategories.Count);
        }
    }
}
