﻿using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using Xunit;

namespace XUnitTest
{
    public class CommonService_LocaleNameToId
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                    .UseInMemoryDatabase(databaseName: "test")
                    .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void LocaleNameToId_Nonexistant()
        {
            int localeId;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                var service = new CommonService(context);
                localeId = service.LocaleNameToId("Berlin");
            }
            Assert.Equal(-1, localeId);
        }

        [Fact]
        public void LocaleNameToId_Existant()
        {
            int localeId;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                var service = new CommonService(context);
                localeId = service.LocaleNameToId("East");
            }
            Assert.Equal(2, localeId);
        }
    }
}
