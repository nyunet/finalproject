﻿using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using Xunit;

namespace XUnitTest
{
    public class CommonService_AreaExists
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                    .UseInMemoryDatabase(databaseName: "test")
                    .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void AreaExists_False()
        {
            bool exists;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                var service = new CommonService(context);
                exists = service.AreaExists(2);
            }
            Assert.False(exists);
        }

        [Fact]
        public void AreaExists_True()
        {
            bool exists;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                var service = new CommonService(context);
                exists = service.AreaExists(1);
            }
            Assert.True(exists);
        }
    }
}
