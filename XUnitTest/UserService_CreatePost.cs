using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest
{
    public class UserService_CreatePost
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                   .UseInMemoryDatabase(databaseName: "test")
                   .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void CreatePostSuccess()
        {
            Post p;
            Outcome outcome;

            string Title = "Dell Laptop E2100";
            string Body = "It's a great gaming laptop. It has Intel inside";
            string OwnerId = "b";
            int AreaId = 1, LocaleId = 3;
            int CategoryId = 1, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");

                var service = new UserService(context);
                outcome = service.CreatePost(Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("", outcome.Message());
            Assert.False(outcome.fail);
            Assert.NotNull(p);
            Assert.Equal(DateTime.Now.Date, p.Timestamp.Date);
            Assert.Equal(DateTime.Now.Hour, p.Timestamp.Hour);
            Assert.Equal(DateTime.Now.Minute, p.Timestamp.Minute);
            Assert.Equal(432000, p.Expiration.Subtract(p.Timestamp).TotalSeconds);
            Assert.Equal("Dell Laptop E2100", p.Title);
            Assert.Equal("It's a great gaming laptop. It has Intel inside", p.Body);
            Assert.Equal("b", p.OwnerId);
            Assert.Equal(1, p.AreaId);
            Assert.Equal(3, p.LocaleId);
            Assert.Equal(1, p.CategoryId);
            Assert.Equal(2, p.SubcategoryId);
        }

        [Fact]
        public void CreatePostFail_NoUser()
        {
            Post p;
            Outcome outcome;

            string Title = "Dell Laptop E2100";
            string Body = "It's a great gaming laptop. It has Intel inside";
            string OwnerId = "nonExistantId";
            int AreaId = 1, LocaleId = 3;
            int CategoryId = 1, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");

                var service = new UserService(context);
                outcome = service.CreatePost(Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("Post owner ID does not correspond to a user", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Null(p);
        }

        [Fact]
        public void CreatePostFail_WrongArea()
        {
            Post p;
            Outcome outcome;

            string Title = "Dell Laptop E2100";
            string Body = "It's a great gaming laptop. It has Intel inside";
            string OwnerId = "b";
            int AreaId = 10, LocaleId = 3;
            int CategoryId = 1, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");

                var service = new UserService(context);
                outcome = service.CreatePost(Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("Area ID does not correspond to an area", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Null(p);
        }

        [Fact]
        public void CreatePostFail_WrongLocale()
        {
            Post p;
            Outcome outcome;

            string Title = "Dell Laptop E2100";
            string Body = "It's a great gaming laptop. It has Intel inside";
            string OwnerId = "b";
            int AreaId = 1, LocaleId = 10;
            int CategoryId = 1, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");

                var service = new UserService(context);
                outcome = service.CreatePost(Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("Locale ID does not correspond to a locale", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Null(p);
        }

        [Fact]
        public void CreatePostFail_WrongCategory()
        {
            Post p;
            Outcome outcome;

            string Title = "Dell Laptop E2100";
            string Body = "It's a great gaming laptop. It has Intel inside";
            string OwnerId = "b";
            int AreaId = 1, LocaleId = 3;
            int CategoryId = 10, SubcategoryId = 2;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");

                var service = new UserService(context);
                outcome = service.CreatePost(Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("Category ID does not correspond to a category", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Null(p);
        }

        [Fact]
        public void CreatePostFail_WrongSubcategory()
        {
            Post p;
            Outcome outcome;

            string Title = "Dell Laptop E2100";
            string Body = "It's a great gaming laptop. It has Intel inside";
            string OwnerId = "b";
            int AreaId = 1, LocaleId = 3;
            int CategoryId = 1, SubcategoryId = 10;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");

                var service = new UserService(context);
                outcome = service.CreatePost(Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
                p = context.Posts.SingleOrDefault();
            }
            Assert.Equal("Subcategory ID does not correspond to a subcategory", outcome.Message());
            Assert.True(outcome.fail);
            Assert.Null(p);
        }

        // TODO: Check data annotations
    }
}
