using BusinessLogic;
using DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest
{
    public class UserService_GetMessagesByUser
    {
        private Tools tools = new Tools();
        private BusinessLogicTestHelper helper = new BusinessLogicTestHelper();

        private static DbContextOptions<BusinessDbContext> CreateTemporaryMemoryOptions()
        {
            // Create a fresh service provider, and therefore a fresh InMemory database instance.
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            // Create a new options instance telling the context to use an
            // InMemory database and the new service provider.
            var builder = new DbContextOptionsBuilder<BusinessDbContext>()
                   .UseInMemoryDatabase(databaseName: "test")
                   .UseInternalServiceProvider(serviceProvider);
            return builder.Options;
        }

        [Fact]
        public void GetMessagesByUser_Multiple()
        {
            List<Message> messages;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 2, 1, 2);
                helper.CreateMessage(context, "My response is awesome", "b","c", 1);
                helper.CreateMessage(context, "My second response is even better", "b", "c", 1);

                var service = new UserService(context);
                messages = service.GetMessagesReceived("b");
            }
            Assert.Equal(2, messages.Count);
            foreach (Message m in messages)
            {
                Assert.NotNull(m);
                Assert.Equal("b", m.SenderId);
                Assert.Equal(1, m.PostId);
            }
        }

        [Fact]
        public void GetMessagesByUser_Single()
        {
            List<Message> messages;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 2, 1, 2);
                helper.CreateMessage(context, "My response is awesome", "b","c", 1);

                var service = new UserService(context);
                messages = service.GetMessagesReceived("b");
            }
            Assert.Single(messages);
            Message m = messages[0];
            Assert.NotNull(m);
            Assert.Equal("b", m.ReceiverId);
            Assert.Equal(1, m.PostId);
            Assert.Equal("My response is awesome", m.Response);
        }

        [Fact]
        public void GetMessagesByUser_None()
        {
            List<Message> messages;
            using (var context = new BusinessDbContext(CreateTemporaryMemoryOptions()))
            {
                helper.CreatePopulateArea(context, "NYC", new List<string> { "West", "East", "North" });
                helper.CreatePopulateCategory(context, "Electronics", new List<string> { "Desktops", "Laptops" });
                helper.CreateUser(context, "b", "user", "John");
                helper.CreatePost(context, "Title1", "Body1", "b", 1, 2, 1, 2);

                var service = new UserService(context);
                messages = service.GetMessagesReceived("b");
            }
            Assert.Empty(messages);
        }
    }
}
