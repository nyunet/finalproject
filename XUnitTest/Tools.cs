﻿using System;
using System.Linq;

namespace XUnitTest
{
    class Tools
    {
        private Random random = new Random();
        public string RandomDbName()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 30)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
