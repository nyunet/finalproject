﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic
{
    public class Outcome
    {
        public bool fail;
        private string _message;

        public string Message()
        {
            return _message;
        }

        public Outcome(string message = "", bool failed = false)
        {
            _message = message;
            fail = failed;
        }
    }
}
