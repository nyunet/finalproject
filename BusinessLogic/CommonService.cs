﻿using DataModel;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BusinessLogic
{
    public class CommonService
    {
        private BusinessDbContext _context;

        public CommonService(BusinessDbContext context)
        {
            _context = context;
        }

        public bool UserExists(string UserId)
        {
            User user = _context.Users.Find(UserId);
            if (user == null || (user.Type != "user" && user.Type != "admin"))
            {
                return false;
            }
            return true;
        }

        public bool PostExists(int PostId)
        {
            if (_context.Posts.Find(PostId) == null)
            {
                return false;
            }
            return true;
        }

        public bool AreaExists(int AreaId)
        {
            if (_context.Areas.Find(AreaId) == null)
            {
                return false;
            }
            return true;
        }

        public bool LocaleExists(int LocaleId)
        {
            if (_context.Locales.Find(LocaleId) == null)
            {
                return false;
            }
            return true;
        }

        public bool CategoryExists(int CategoryId)
        {
            if (_context.Categories.Find(CategoryId) == null)
            {
                return false;
            }
            return true;
        }

        public bool SubcategoryExists(int SubcategoryId)
        {
            if (_context.Subcategories.Find(SubcategoryId) == null)
            {
                return false;
            }
            return true;
        }

        public Outcome CreateUser(string UserId, string Name)
        {
            if (Name.Length < 3 || Name.Length > 30)
            {
                return new Outcome("The username must be between 3 and 30 characters", true);
            }
            string Type = "user";
            if (_context.Users.ToList().Count == 0) // First user
            {
                Type = "admin";
            }
            User u = new User { Id = UserId, Name = Name, Type = Type };
            _context.Users.Add(u);
            _context.SaveChanges();
            return new Outcome();
        }

        public string UserNameToId (string name)
        {
            User user = _context.Users.Where(u => u.Name == name).SingleOrDefault();
            if (user == null)
            {
                return "";
            }
            return user.Id;
        }
        public Post GetPostById(int? id)
        {
            Post post = _context.Posts.Include(p => p.Owner).Where(p => p.Id == id).SingleOrDefault();
            return post;
        
        }

        public int CategoryNameToId(string Name)
        // -1 if not found
        {
            Category x = _context.Categories.Where(y => y.Name == Name).SingleOrDefault();
            if (x == null)
            {
                return -1;
            }
            return x.Id;
        }

        public int SubcategoryNameToId(string Name)
        // -1 if not found
        {
            Subcategory x = _context.Subcategories.Where(y => y.Name == Name).SingleOrDefault();
            if (x == null)
            {
                return -1;
            }
            return x.Id;
        }

        public int AreaNameToId(string Name)
        // -1 if not found
        {
            Area x = _context.Areas.Where(y => y.Name == Name).SingleOrDefault();
            if (x == null)
            {
                return -1;
            }
            return x.Id;
        }

        public int LocaleNameToId(string Name)
        // -1 if not found
        {
            Locale x = _context.Locales.Where(y => y.Name == Name).SingleOrDefault();
            if (x == null)
            {
                return -1;
            }
            return x.Id;
        }

        public List<Area> ListAreas()
        {
            return _context.Areas.ToList();
        }

        public List<Locale> ListLocales(int areaId)
        {
            return _context.Locales.Where(l => l.AreaId == areaId).ToList();
        }

        public List<Category> ListCategories()
        {
            return _context.Categories.ToList();
        }

        public List<Category> ListUnhiddenCategories()
        {
            return _context.Categories.Where(c => !c.Hidden).ToList();
        }

        public List<Area> ListUnhiddenAreas()
        {
            return _context.Areas.Where(a => !a.Hidden).ToList();
        }

        public List<Subcategory> ListSubcategories(int categoryId)
        {
            return _context.Subcategories.Where(l => l.CategoryId == categoryId).ToList();
        }

        public string AddSlug(string str)
        {
            return str.ToLower().Replace(" ", "-");
        }
        public string RemoveSlug(string str)
        {
            return str.Replace("-", " ");
        }

        public Category GetCategory(int Id)
        {
            return _context.Categories.Find(Id);
        }
        
        public Subcategory GetSubcategory(int Id)
        {
            return _context.Subcategories.Find(Id);
        }

        public Category GetCategoryFromSlug(string categorySlug)
        {
            return _context.Categories.Where(c => c.GetSlug() == categorySlug).SingleOrDefault();
        }

        public Subcategory GetSubcategoryFromSlug(string subcategorySlug)
        {
            return _context.Subcategories.Where(sc => sc.GetSlug() == subcategorySlug).SingleOrDefault();
        }

        public Area GetAreaFromSlug(string areaSlug)
        {
            return _context.Areas.Where(c => c.GetSlug() == areaSlug).SingleOrDefault();
        }

        public Locale GetLocaleFromSlug(string localeSlug)
        {
            return _context.Locales.Where(sc => sc.GetSlug() == localeSlug).SingleOrDefault();
        }

        public User GetUser(string id)
        {
            return _context.Users.Find(id);
        }

        public Area GetArea(int id)
        {
            return _context.Areas.Find(id);
        }

        public Locale GetLocale(int id)
        {
            return _context.Locales.Find(id);
        }
    }
}
