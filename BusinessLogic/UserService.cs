﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using DataModel;

namespace BusinessLogic
{
    public class UserService
    {
        private static BusinessDbContext _context;
        private CommonService commonService;

        public UserService(BusinessDbContext context)
        {
            _context = context;
            commonService = new CommonService(_context);
        }

        public Outcome CreatePost(string Title, string Body, string OwnerId,
            int AreaId, int LocaleId, int CategoryId, int SubcategoryId)
        {
            if (Title.Length < 3 || Title.Length > 30)
            {
                return new Outcome("The title must be at between 3 and 30 characters", true);
            }
            if (Body.Length < 10 || Body.Length > 600)
            {
                return new Outcome("The description must be at between 10 and 600 characters", true);
            }
            // We assume this action is authorized for this OwnerId
            if (!commonService.UserExists(OwnerId))
            {
                return new Outcome("Post owner ID does not correspond to a user", true);
            }
            if (!commonService.AreaExists(AreaId))
            {
                return new Outcome("Area ID does not correspond to an area", true);
            }
            if (!commonService.LocaleExists(LocaleId))
            {
                return new Outcome("Locale ID does not correspond to a locale", true);
            }
            if (!commonService.CategoryExists(CategoryId))
            {
                return new Outcome("Category ID does not correspond to a category", true);
            }
            if (!commonService.SubcategoryExists(SubcategoryId))
            {
                return new Outcome("Subcategory ID does not correspond to a subcategory", true);
            }
            Post p = new Post
            {
                Title = Title,
                Body = Body,
                Owner = _context.Users.Where(u => u.Id == OwnerId).SingleOrDefault(),
                OwnerId = OwnerId,
                Area = _context.Areas.Where(u => u.Id == AreaId).SingleOrDefault(),
                AreaId = AreaId,
                Locale = _context.Locales.Where(u => u.Id == LocaleId).SingleOrDefault(),
                LocaleId = LocaleId,
                Category = _context.Categories.Where(u => u.Id == CategoryId).SingleOrDefault(),
                CategoryId = CategoryId,
                Subcategory = _context.Subcategories.Where(u => u.Id == SubcategoryId).SingleOrDefault(),
                SubcategoryId = SubcategoryId
            };
            _context.Posts.Add(p);
            _context.SaveChanges();
            return new Outcome();
        }

        public Outcome ModifyPost(int PostId, string Title = "", string Body = "",
            string OwnerId = "", int AreaId = -1, int LocaleId = -1, int CategoryId = -1,
            int SubcategoryId = -1)
        {
            // Make sure ownerID is the owner of the post beforehand
            Post p = _context.Posts.Find(PostId);
            if (p == null)
            {
                return new Outcome("Post ID does not correspond to a post", true);
            }

            // Checking parameters given
            if (OwnerId != "" && !commonService.UserExists(OwnerId))
            {
                return new Outcome("Post owner ID does not correspond to a user", true);
            }
            if (AreaId > -1 && !commonService.AreaExists(AreaId))
            {
                return new Outcome("Area ID does not correspond to an area", true);
            }
            // TODO check localeId match AreaId, same for subcategory
            if (LocaleId > -1 && !commonService.LocaleExists(LocaleId))
            {
                return new Outcome("Locale ID does not correspond to a locale", true);
            }
            if (CategoryId > -1 && !commonService.CategoryExists(CategoryId))
            {
                return new Outcome("Category ID does not correspond to a category", true);
            }
            if (SubcategoryId > -1 && !commonService.SubcategoryExists(SubcategoryId))
            {
                return new Outcome("Subcategory ID does not correspond to a subcategory", true);
            }

            // Setting the parameters given
            if (Title != "")
            {
                p.Title = Title;
            }
            if (Body != "")
            {
                p.Body = Body;
            }
            if (OwnerId != "")
            {
                p.OwnerId = OwnerId;
            }
            if (AreaId > -1)
            {
                p.AreaId = AreaId;
            }
            if (LocaleId > -1)
            {
                p.LocaleId = LocaleId;
            }
            if (CategoryId > -1)
            {
                p.CategoryId = CategoryId;
            }
            if (SubcategoryId > -1)
            {
                p.SubcategoryId = SubcategoryId;
            }
            p.Timestamp = DateTime.Now;
            _context.SaveChanges();
            return new Outcome();
        }

        public Outcome DeleteMyPost(int PostId, string UserId)
        {
            Post post = _context.Posts.Where(p => p.Id == PostId).SingleOrDefault();
            if (post == null)
            {
                return new Outcome("Post ID does not exist", true);
            }
            if (post.Owner.Id != UserId)
            {
                return new Outcome("User ID does not correspond to the Post Owner ID", true);
            }

            post.Active = false;
            _context.SaveChanges();
            return new Outcome();
        }
        public List<Post> GetExpiredPosts(string UserId, DateTime now)
        {
            List<Post> expiredPosts = _context.Posts
                .Include(p => p.Subcategory)
                .Include(p => p.Category)
                .Include(p => p.Area)
                .Include(p => p.Locale)
                .Include(p => p.Owner)
                .Where(p => p.OwnerId == UserId && p.Expiration <= now).ToList();
            return expiredPosts;
        }

        public List<Post> GetUnexpiredPosts(string UserId, DateTime now)
        {
            List<Post> posts = _context.Posts
                .Include(p => p.Subcategory )
                .Include(p => p.Category)
                .Include(p => p.Area)
                .Include(p => p.Locale)
                .Include(p => p.Owner)
                .Where(p => p.OwnerId == UserId && p.Expiration > now && p.Active).ToList();
            return posts;
        }

        public Outcome CreateMessage(int PostId, string SenderId, string ReceiverId, string Response)
        {
            if (Response.Length < 10 || Response.Length > 600)
            {
                return new Outcome("The Response must be between 10 and 600 characters", true);
            }
            if (!commonService.UserExists(SenderId))
            {
                return new Outcome("User ID does not correspond to a user", true);
            }
            if (!commonService.UserExists(ReceiverId))
            {
                return new Outcome("User ID does not correspond to a user", true);
            }
            if (!commonService.PostExists(PostId))
            {
                return new Outcome("Post ID does not correspond to a post", true);
            }
            Message m = new Message { PostId = PostId,
                                      SenderId = SenderId,
                                      Sender = _context.Users.Where(u => u.Id == SenderId).SingleOrDefault(),
                                      ReceiverId = ReceiverId,
                                      //Receiver = _context.Users.Where(u => u.Id == ReceiverId).SingleOrDefault(),
                                      Response = Response };
            _context.Messages.Add(m);
            _context.SaveChanges();
            return new Outcome(); // success
        }

        public List<Message> GetMessagesReceived(string UserId)
        {
            List<Message> messages = _context.Messages
                .Include(m => m.Post)
                .Include(m => m.Receiver)
                .Include(m => m.Sender)
                .Where(m => m.ReceiverId == UserId).ToList();
            return messages;
        }
        public Message GetMessageById(int? id)
        {
           Message message = _context.Messages
                .Include(m => m.Post)
                .Include(m => m.Receiver)
                .Include(m => m.Sender)
                .Where(m => m.Id == id).SingleOrDefault();
            return message;
        }
        public List<Message> GetMessagesSent(string UserId)
        {
            List<Message> messages = _context.Messages
                .Include(m => m.Post)
                .Include(m => m.Receiver)
                .Include(m => m.Sender)
                .Where(m => m.SenderId == UserId).ToList();
            return messages;
        }

        public List<Message> GetMessagesByPost(int PostId)
        {
            List<Message> messages = _context.Messages.Where(m => m.PostId == PostId).ToList();
            return messages;
        }

        public List<Post> SearchPostsByKeywords(string queryString)
        {
            List<Post> matchingPosts = new List<Post>{};
            queryString = queryString.ToLower();
            // Splits query string by space and remove empty spaces
            string[] words = queryString.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
            if (words.Length == 0)
            {
                matchingPosts = _context.Posts.ToList();
                return matchingPosts;
            }
            matchingPosts = _context.Posts.Where(p => (p.Title.ToLower().Contains(words[0]) || p.Body.ToLower().Contains(words[0]))).ToList();
            for (uint i = 1; i < words.Length; i++)
            {
                matchingPosts.RemoveAll(p => !p.Title.ToLower().Contains(words[i]) && !p.Body.ToLower().Contains(words[i]));
            }
            return matchingPosts;
        }

        public List<Post> SearchPostsByFilters(int categoryId = -1, 
            int subcategoryId = -1, int areaId = -1, int localeId = -1)
        {
            // TODO: On UI, only allow subcategory if category is present; only allow locale if area is present
            List<Post> allPosts;
            allPosts = _context.Posts.ToList();
            return FilterPosts(allPosts, categoryId, subcategoryId, areaId, localeId);
        }

        public List<Post> FilterPosts(List<Post> posts, int categoryId = -1, int subcategoryId = -1, int areaId = -1, int localeId = -1)
        {
            if (categoryId > -1) // Category filter is set
            {
                posts.RemoveAll(p => p.CategoryId != categoryId);
                if (subcategoryId > -1)
                {
                    posts.RemoveAll(p => p.SubcategoryId != subcategoryId);
                }
            }
            if (areaId > -1) // Area filter is set
            {
                posts.RemoveAll(p => p.AreaId != areaId);
                if (localeId > -1)
                {
                    posts.RemoveAll(p => p.LocaleId != localeId);
                }
            }
            foreach (Post post in posts)
            {
                post.Locale = _context.Locales.Find(post.LocaleId);
            }
            return posts.Where(p => DateTime.Now < p.Expiration && p.Active)
                        .OrderByDescending(p => p.Timestamp)
                        .ToList();
        }
    }
}
