﻿using DataModel;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BusinessLogic
{
    public class AdminService
    {
        private static BusinessDbContext _context;
        private CommonService commonService;
        private UserService userService;

        public AdminService(BusinessDbContext context)
        {
            _context = context;
            commonService = new CommonService(_context);
            userService = new UserService(_context);
        }

        public bool IsAdminValid(string AdminId)
        {
            User admin = _context.Users.Find(AdminId);
            if (admin == null || admin.Type != "admin")
            {
                return false;
            }
            return true;
        }

        public Outcome PromoteUser(string AdminId, string UserId)
        {
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            User user = _context.Users.Find(UserId);
            if (user == null || user.Type != "user")
            {
                return new Outcome("User ID does not correspond to a user", true);
            }
            user.Type = "admin";
            _context.SaveChanges();
            return new Outcome(); // no error
        }

        public List<Category> ListAllCategories(string AdminId)
        {
            if (!IsAdminValid(AdminId))
            {
                return null; // error
            }
            return _context.Categories.Include(cat => cat.Subcategories).ToList();
        }

        public Outcome CreateCategory(string AdminId, string CategoryName)
        {
            if (CategoryName == "")
            {
                return new Outcome("Category Name is required", true);
            }
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            if (commonService.CategoryNameToId(CategoryName) > -1) {
                return new Outcome("Category name already exists", true);
            }
            Category c = new Category { Name = CategoryName };
            _context.Categories.Add(c);
            _context.SaveChanges();
            return new Outcome(); // no error
        }

        public Outcome ModifyCategory(string AdminId, int CategoryId, string CategoryName)
        {
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            Category c = _context.Categories.Find(CategoryId);
            if (c == null)
            {
                return new Outcome("Category ID does not correspond to an existing category", true);
            }
            c.Name = CategoryName;
            _context.SaveChanges();
            return new Outcome();
        }

        public Outcome HideCategory(string AdminId, int CategoryId)
        {
            // HideCategory also hides all corresponding subcategories
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            Category c = _context.Categories.Find(CategoryId);
            if (c == null)
            {
                return new Outcome("Category ID does not correspond to an existing category", true);
            }
            c.Hidden = true;
            List<Subcategory> subcategories = _context.Subcategories.Where(sc => sc.CategoryId == CategoryId).ToList();
            foreach (Subcategory sc in subcategories)
            {
                sc.Hidden = true;
            }
            _context.SaveChanges();
            return new Outcome();
        }

        public List<Subcategory> ListAllSubcategories(string AdminId)
        {
            if (!IsAdminValid(AdminId))
            {
                return null; // error
            }
            return _context.Subcategories.Include(sub => sub.Category).ToList();
        }

        public Outcome CreateSubcategory(string AdminId, string SubcategoryName, int CategoryId)
        {
            if (SubcategoryName == "")
            {
                return new Outcome("Subcategory name is required", true);
            }
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            Category cat = _context.Categories.Where(c => c.Id == CategoryId).SingleOrDefault();
            if (cat == null)
            {
                return new Outcome("Category ID does not correspond to an existing category", true);
            }
            int subcId = commonService.SubcategoryNameToId(SubcategoryName);
            if (subcId > -1 && _context.Subcategories.Find(subcId).CategoryId == CategoryId)
            {
                return new Outcome("Subcategory name already exists for this category", true);
            }
            Subcategory sc = new Subcategory {
                Name = SubcategoryName,
                CategoryId = CategoryId,
                Category = cat
            };
            _context.Subcategories.Add(sc);
            _context.SaveChanges();
            return new Outcome();
        }

        public Outcome ModifySubcategory(string AdminId, int SubcategoryId, string Name = "", int CategoryId = -1)
        {
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            Subcategory sc = _context.Subcategories.Find(SubcategoryId);
            if (sc == null)
            {
                return new Outcome("Subcategory ID does not correspond to an existing subcategory", true);
            }
            if (CategoryId > -1)
            {
                if (!commonService.CategoryExists(CategoryId))
                {
                    return new Outcome("Category ID does not correspond to an existing category", true);
                }
                sc.CategoryId = CategoryId;
            }
            if (Name != "")
            {
                sc.Name = Name;
            }
            _context.SaveChanges();
            return new Outcome();
        }

        public Outcome HideSubcategory(string AdminId, int SubcategoryId)
        {
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            Subcategory sc = _context.Subcategories.Find(SubcategoryId);
            if (sc == null)
            {
                return new Outcome("Subcategory ID does not correspond to an existing subcategory", true);
            }
            sc.Hidden = true;
            _context.SaveChanges();
            return new Outcome();
        }

        // TODO Maybe do unhide ?
        public List<Area> ListAllAreas(string AdminId)
        {
            if (!IsAdminValid(AdminId))
            {
                return null; // error
            }
            return _context.Areas.Include(area => area.Locales).ToList();
        }

        public Outcome CreateArea(string AdminId, string AreaName)
        {
            if (AreaName == "")
            {
                return new Outcome("Area name is required", true);
            }
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            if (commonService.AreaNameToId(AreaName) > -1)
            {
                return new Outcome("Area name already exists", true);
            }
            Area a = new Area { Name = AreaName };
            _context.Areas.Add(a);
            _context.SaveChanges();
            return new Outcome(); // no error
        }

        public Outcome ModifyArea(string AdminId, int AreaId, string Name)
        {
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            Area a = _context.Areas.Find(AreaId);
            if (a == null)
            {
                return new Outcome("Area ID does not correspond to an existing area", true);
            }
            a.Name = Name;
            _context.SaveChanges();
            return new Outcome();
        }

        public Outcome HideArea(string AdminId, int AreaId)
            // HideArea also hides all corresponding locales
        {
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            Area a = _context.Areas.Find(AreaId);
            if (a == null)
            {
                return new Outcome("Area ID does not correspond to an existing area", true);
            }
            a.Hidden = true;
            List<Locale> locales = _context.Locales.Include(l => l.Area).Where(l => l.AreaId == AreaId).ToList();
            foreach(Locale l in locales)
            {
                l.Hidden = true;
            }
            _context.SaveChanges();
            return new Outcome();
        }

        public List<Locale> ListAllLocales(string AdminId)
        {
            if (!IsAdminValid(AdminId))
            {
                return null; // error
            }
            return _context.Locales.Include(l => l.Area).ToList();
        }

        public Outcome CreateLocale(string AdminId, string LocaleName, int AreaId)
        {
            if (LocaleName == "")
            {
                return new Outcome("Locale Name is required", true);
            }
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            if (!commonService.AreaExists(AreaId))
            {
                return new Outcome("Area ID does not correspond to an existing area", true);
            }
            int localeId = commonService.LocaleNameToId(LocaleName);
            if (localeId > -1 && _context.Locales.Find(localeId).AreaId == AreaId)
            {
                return new Outcome("Locale name already exists for this area", true);
            }
            Locale l = new Locale { Name = LocaleName, AreaId = AreaId };
            _context.Locales.Add(l);
            _context.SaveChanges();
            return new Outcome();
        }

        public Outcome ModifyLocale(string AdminId, int LocaleId, string Name = "", int AreaId = -1)
        {
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            Locale l = _context.Locales.Find(LocaleId);
            if (l == null)
            {
                return new Outcome("Locale ID does not correspond to an existing locale", true);
            }
            if (AreaId > -1)
            {
                if (!commonService.AreaExists(AreaId))
                {
                    return new Outcome("Area ID does not correspond to an existing area", true);
                }
                l.AreaId = AreaId;
            }
            if (Name != "")
            {
                l.Name = Name;
            }
            _context.SaveChanges();
            return new Outcome();
        }

        public Outcome HideLocale(string AdminId, int LocaleId)
        {
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            Locale l = _context.Locales.Find(LocaleId);
            if (l == null)
            {
                return new Outcome("Locale ID does not correspond to an existing locale", true);
            }
            l.Hidden = true;
            _context.SaveChanges();
            return new Outcome();
        }
        //For regular user list of posts
        public List<Post> ListPosts(string AdminId, string UserId)
        {
            if (!IsAdminValid(AdminId))
            {
                return null; // error
            }
            return _context.Posts.Where(p => p.OwnerId == UserId).Include(posts => posts.Area)
                                                                 .Include(posts => posts.Category)
                                                                 .Include(posts => posts.Locale)
                                                                 .Include(posts => posts.Subcategory)
                                                                 .Include(posts => posts.Owner)
                                                                 .OrderByDescending(p => p.Timestamp).ToList();
        }
        //For Admin user list of posts.
        public List<Post> ListPosts(string AdminId)
        {
            if (!IsAdminValid(AdminId))
            {
                return null; // error
            }
            return _context.Posts.Include(posts => posts.Area)
                                 .Include(posts => posts.Category)
                                 .Include(posts => posts.Locale)
                                 .Include(posts => posts.Subcategory)
                                 .Include(posts => posts.Owner)
                                 .OrderByDescending(p => p.Timestamp).ToList();
        }

        public List<User> ListUsers(string AdminId)
        {
            if (!IsAdminValid(AdminId))
            {
                return null; // error
            }
            return _context.Users.ToList();
        }

        public Outcome CreatePost(string AdminId, string Title, string Body, string OwnerId,
            int AreaId, int LocaleId, int CategoryId, int SubcategoryId)
        {
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            return userService.CreatePost(Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
        }

        public Outcome ModifyPost(string AdminId, int PostId, string Title = "", string Body = "", 
            string OwnerId = "", int AreaId = -1, int LocaleId = -1, int CategoryId = -1, 
            int SubcategoryId = -1)
        {
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            return userService.ModifyPost(PostId, Title, Body, OwnerId, AreaId, LocaleId, CategoryId, SubcategoryId);
        }

        public Outcome DeletePost(string AdminId, int PostId)
        {
            if (!IsAdminValid(AdminId))
            {
                return new Outcome("User ID does not correspond to an administrator", true);
            }
            Post p = _context.Posts.Find(PostId);
            if (p != null)
            {
                return new Outcome("Post ID does not correspond to a post", true);
            }
            _context.Posts.Remove(p);
            _context.SaveChanges();
            return new Outcome();
        }

    }
}
