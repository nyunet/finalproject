﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using BusinessLogic;
using DataModel;
using Mvc.Data;
using Mvc.Models;
using Mvc.Services;

namespace Mvc
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Production")
            {
                services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("AzureDatabaseConnection")));

                services.AddDbContext<BusinessDbContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("AzureDatabaseConnection"), b => b.MigrationsAssembly("DataModel")));
            }
            else
            {
                services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

                services.AddDbContext<BusinessDbContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("DataModel")));
            }

            // Automatically perform database migration
            services.BuildServiceProvider().GetService<ApplicationDbContext>().Database.Migrate();
            services.BuildServiceProvider().GetService<BusinessDbContext>().Database.Migrate();

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddScoped<UserService>();
            services.AddScoped<AdminService>();
            services.AddScoped<CommonService>();
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddMvc();
            //services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseSession();

            app.UseMvc(routes =>
            {
                //TODO complete routes for Area, Locale, Category Subcategory
                routes.MapRoute(
                    name: "LocaleView",
                    template: "browse/{area}/{locale?}",
                    defaults: new { controller = "AreaLocale", action = "Index" }
                );
                routes.MapRoute(
                    name: "ListPosts",
                    template: "browse/{area}/{locale}/{category}/search",
                    defaults: new { controller = "CategorySubCategory", action = "Index" }
                );
                routes.MapRoute(
                    name: "FilteredListPosts",
                    template: "search/{area}/{locale?}/{category}/{subcategory?}",
                    defaults: new { controller = "CategorySubCategory", action = "Index" }
                );
                routes.MapRoute(
                    name: "KeywordSearch",
                    template: "keyword/search/{action=keywordsearch}",
                    defaults: new { controller = "CategorySubCategory" }
                );

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "AdminCreatePosts",
                    template: "Admin/{controller=Post}/{action=AdminCreatePost}"
                );
            });
        }
    }
}
