﻿// Write your JavaScript code.
function FillLocales() {
    var areaId = $('#AreaId').val();
    $.ajax({
        url: '/Post/FillLocales/' + areaId,
        type: "GET",
        dataType: "JSON",
        data: { AreaId: areaId },
        success: function (Locales) {
            $("#LocaleId").html(""); // clear before appending new list
            $.each(Locales, function (i, locale) {
                $("#LocaleId").append(
                    $('<option></option>').val(locale.id).html(locale.name));
            });
        }
    });
}

function FillSubcategories() {
    var categoryId = $('#CategoryId').val();
    $.ajax({
        url: '/Post/FillSubcategories/' + categoryId,
        type: "GET",
        dataType: "JSON",
        data: { CategoryId: categoryId },
        success: function (subcategories) {
            $("#SubcategoryId").html(""); // clear before appending new list
            $.each(subcategories, function (i, subcategory) {
                $("#SubcategoryId").append(
                    $('<option></option>').val(subcategory.id).html(subcategory.name));
            });
        }
    });
}
