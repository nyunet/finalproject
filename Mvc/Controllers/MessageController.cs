﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using BusinessLogic;
using DataModel;

namespace Mvc.Controllers
{
    public class MessageController : Controller
    {
        private BusinessDbContext dataDb;
        private CommonService _commonService;
        private UserService _userService;
        private AdminService _adminService;
        public MessageController(BusinessDbContext businessDbContext, CommonService commonService, UserService userService, AdminService adminService)
        {
            dataDb = businessDbContext;
            _commonService = commonService;
            _userService = userService;
            _adminService = adminService;
        }

        [HttpGet]
        public IActionResult Inbox()
        {
            if (User.Identity.IsAuthenticated)
            {
                string UserId = _commonService.UserNameToId(User.Identity.Name);
                return View(_userService.GetMessagesReceived(UserId));
            }
            return View("AccessDenied");
        }

        [HttpGet]
        public IActionResult SentMessages()
        {
            if (User.Identity.IsAuthenticated)
            {
                string UserId = _commonService.UserNameToId(User.Identity.Name);
                return View(_userService.GetMessagesSent(UserId));
            }
            return View("AccessDenied");
        }

        [HttpGet]
        public IActionResult SendMessage(int? id, int? messageId, int? sentBox)
        {
            if (User.Identity.IsAuthenticated)
            {
                string UserId = _commonService.UserNameToId(User.Identity.Name);
                ViewBag.SenderId = UserId;
                Post post = _commonService.GetPostById(id);
                if (post ==null)
                {
                    return View("AccessDenied");
                }
                List<Post> posts = new List<Post>();
                List<User> Recipients = new List<User>();

                posts.Add(post);

                if (messageId == null)
                {
                    Recipients.Add(post.Owner);
                }
                else
                {
                    //To reply to a message
                    Message message = _userService.GetMessageById(messageId);
                    if (sentBox == null)
                    {
                        Recipients.Add(message.Sender);
                    }
                    else
                    {
                        Recipients.Add(message.Receiver);
                    }
                }
                
                ViewBag.Receiver = Recipients;
                ViewBag.Post = posts;
                
                return View();
            }
            return View("AccessDenied");
        }

        [HttpPost]
        public IActionResult SendMessage(Message message)
        {
            if (ModelState.IsValid && User.Identity.IsAuthenticated)
            {
                Outcome response = _userService.CreateMessage(message.PostId, 
                                                              message.SenderId, 
                                                              message.ReceiverId,
                                                              message.Response);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return RedirectToAction("SentMessages");
            }
            return View("AccessDenied");
        }

        [HttpGet]
        public IActionResult FullMessage(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                Message message = _userService.GetMessageById(id);
                return View(message);
            }
            return View("AccessDenied");
        }
    }
}