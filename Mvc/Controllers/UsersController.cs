﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BusinessLogic;
using Mvc.Models;
using DataModel;
using Mvc.Data;

namespace Mvc.Controllers
{
    public class UsersController : Controller
    {
        private readonly BusinessDbContext _businessContext;
        private readonly ApplicationDbContext _context;
        private readonly UserService _userService;
        private readonly AdminService _adminService;
        private readonly CommonService _commonService;

        public UsersController(ApplicationDbContext appContext, BusinessDbContext busiContext, UserService userService, AdminService adminService, CommonService commonService)
        {
            _context = appContext;
            _businessContext = busiContext;
            _userService = userService;
            _adminService = adminService;
            _commonService = commonService;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            List<AdminUserViewModel> avms = new List<AdminUserViewModel>();
            foreach (ApplicationUser user in await _context.Users.ToListAsync())
            {
                Models.AccountViewModels.RegisterViewModel rgvm = new Models.AccountViewModels.RegisterViewModel { Email = user.Email };
                AdminUserViewModel adminUserView = new AdminUserViewModel {
                    user = _commonService.GetUser(user.Id),
                    registerViewModel = rgvm
                };
                avms.Add(adminUserView);
            }
            return View(avms);
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AdminUserViewModel AuViewModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(AuViewModel.user);
                await _context.SaveChangesAsync();
                _commonService.CreateUser(AuViewModel.user.Id, AuViewModel.user.Name);
                return RedirectToAction(nameof(Index));
            }
            return View(AuViewModel);
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> PromoteUser(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            User user = _commonService.GetUser(id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("PromoteUser")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PromoteUserConfirmed(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                var user = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome result = _adminService.PromoteUser(adminId, id);
                if (result.fail)
                {
                    ViewBag.ErrorMessage = result.Message();
                    return View("AccessDenied");
                }
                _context.Update(user);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                throw;
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var user = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(string id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}
