﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BusinessLogic;
using DataModel;
using X.PagedList;
using Mvc.Models;

namespace Mvc.Controllers
{
    public class CategorySubCategoryController : Controller
    {

        private BusinessDbContext dataDb;
        private readonly AdminService _adminService;
        private readonly CommonService _commonService;
        private readonly UserService _userService;

        public CategorySubCategoryController(BusinessDbContext businessDbContext, AdminService adminService)
        {
            dataDb = businessDbContext;
            _adminService = adminService;
            _commonService = new CommonService(dataDb);
            _userService = new UserService(dataDb);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult KeywordSearch(string area, string locale, string category, string subcategory,
                                             string currentFilter, int? page)
        {
            ViewBag.AreaSlug = area;
            ViewBag.LocaleSlug = locale;
            ViewBag.CategorySlug = category;
            ViewBag.SubCategorySlug = subcategory;

            TempData["AreaSlug"] = area;
            TempData["LocaleSlug"] = locale;
            TempData["CategorySlug"] = category;
            TempData["SubCategorySlug"] = subcategory;
            TempData.Keep();

            List<Post> keywordPosts;
            Category categoryFilter = _commonService.GetCategoryFromSlug(category);
            Subcategory subcategoryFilter = _commonService.GetSubcategoryFromSlug(subcategory);
            Area areaFilter = _commonService.GetAreaFromSlug(area);
            Locale localeFilter = _commonService.GetLocaleFromSlug(locale);
            int categoryFilterId = -1, subcategoryFilterId = -1, areaFilterId = -1, localeFilterId = -1;
            if (categoryFilter != null)
            {
                categoryFilterId = categoryFilter.Id;
            }
            if (subcategoryFilter != null)
            {
                subcategoryFilterId = subcategoryFilter.Id;
            }
            if (areaFilter != null)
            {
                areaFilterId = areaFilter.Id;
            }
            if (localeFilter != null)
            {
                localeFilterId = localeFilter.Id;
            }
            currentFilter = String.IsNullOrEmpty(currentFilter) ? "" : currentFilter;
            keywordPosts = _userService.FilterPosts(_userService.SearchPostsByKeywords(currentFilter), 
                categoryFilterId, subcategoryFilterId, areaFilterId, localeFilterId);
            int pageNumber = page ?? 1;
            ViewBag.Posts = keywordPosts.ToPagedList(pageNumber, 10);
            ViewBag.CurrentFilter = currentFilter;
            ViewBag.Page = pageNumber;
            return View("KeywordSearch");
        }

        [HttpGet]
        public IActionResult CategoryCreate()
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                return View();
            }
            return View("AccessDenied");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CategoryCreate(Category model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.CreateCategory(adminId, model.Name);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return RedirectToAction("CategoryList");
            }
            return View();
        }

        [HttpGet]
        public IActionResult SubCategoryCreate()
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                ViewBag.Categories = _commonService.ListUnhiddenCategories();
                return View();
            }
            return View("AccessDenied");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SubCategoryCreate(AdminUserViewModel model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid && User.Identity.IsAuthenticated)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.CreateSubcategory(adminId, model.subcategory.Name, model.subcategory.CategoryId);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return RedirectToAction("SubCategoryList");
            }
            return View();
        }

        public IActionResult CategoryList()
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                List<Category> categories = dataDb.Categories.Where(a => !a.Hidden).ToList();
                return View(_commonService.ListUnhiddenCategories());
            }
            return View("AccessDenied");
        }

        public IActionResult SubCategoryList()
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                return View(_adminService.ListAllSubcategories(adminId));
            }
            return View("AccessDenied");
        }

        public IActionResult DetailsCategory(int? id)
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                if (id == null)
                {
                    return StatusCode(400);
                }
                Category category = _commonService.GetCategory((int)id);
                if (category == null)
                {
                    return NotFound();
                }
                ViewBag.CategorySubcategories = _commonService.ListSubcategories((int)id);
                return View(category);
            }
            return View("AccessDenied");
        }

        [HttpGet]
        public IActionResult CategoryEdit(int? id)
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                if (id == null)
                {
                    return StatusCode(400);
                }
                Category category = _commonService.GetCategory((int)id);
                return View(category);
            }
            return View("AccessDenied");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CategoryEdit(Category model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.ModifyCategory(adminId, model.Id, model.Name);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return View("CategoryList");
            }
            return View();
        }

        [HttpGet]
        public IActionResult SubCategoryEdit(int? id)
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                if (id == null)
                {
                    return StatusCode(400);
                }
                ViewBag.Categories = _commonService.ListUnhiddenCategories();
                return View(_commonService.GetSubcategory((int)id));
            }
            return View("AccessDenied");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SubCategoryEdit(AdminUserViewModel model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.ModifySubcategory(adminId, 
                    model.subcategory.Id, model.subcategory.Name, model.category.Id);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return View("SubCategoryList");
            }
            return View();
        }

        [HttpGet]
        public IActionResult CategoryDelete(int? id)
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                if (id == null)
                {
                    return StatusCode(400);
                }
                return View(_commonService.GetCategory((int)id));
            }
            return View("AccessDenied");
        }

        [HttpPost, ActionName("CategoryDelete")]
        [ValidateAntiForgeryToken]
        public IActionResult CategoryDeleteConfirmed(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.HideCategory(adminId, id);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return View("CategoryList");
            }
            return View();
        }

        [HttpGet]
        public IActionResult SubCategoryDelete(int? id)
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                if (id == null)
                {
                    return StatusCode(400);
                }
                return View(_commonService.GetSubcategory((int)id));
            }
            return View("AccessDenied");
        }

        [HttpPost, ActionName("SubCategoryDelete")]
        [ValidateAntiForgeryToken]
        public IActionResult SubCategoryDeleteConfirmed(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.HideSubcategory(adminId, id);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return View("SubCategoryList");
            }
            return View();
        }
    }
}