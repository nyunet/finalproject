﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using BusinessLogic;
using DataModel;

namespace Mvc.Controllers
{
    public class PostController : Controller
    {
        private BusinessDbContext dataDb;
        private CommonService _commonService;
        private UserService _userService;
        private AdminService _adminService;

        public PostController(BusinessDbContext businessDbContext, CommonService commonService, UserService userService, AdminService adminService)
        {
            dataDb = businessDbContext;
            _commonService = commonService;
            _userService = userService;
            _adminService = adminService;

        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AdminCreatePost()
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                ViewBag.Users = _adminService.ListUsers(adminId);
                ViewBag.Areas = _adminService.ListAllAreas(adminId);
                ViewBag.Locales = _adminService.ListAllLocales(adminId);
                ViewBag.Categories = _adminService.ListAllCategories(adminId);
                ViewBag.Subcategories = _adminService.ListAllSubcategories(adminId);
                return View();
            }
            return View("AccessDenied");
        }

        [HttpPost]
        public IActionResult AdminCreatePost(Post model)
        {
            if (ModelState.IsValid && User.Identity.IsAuthenticated)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                if (_adminService.IsAdminValid(adminId))
                {
                    Outcome response = _adminService.CreatePost(adminId, 
                                                               model.Title, 
                                                               model.Body, 
                                                               model.OwnerId, 
                                                               model.AreaId, 
                                                               model.LocaleId,
                                                               model.CategoryId,
                                                               model.SubcategoryId);
                    if (response.fail)
                    {
                        ViewBag.ErrorMessage = response.Message();
                        return View("AccessDenied");
                    }
                    return RedirectToAction("KeywordSearch", "CategorySubCategory", new
                    {
                        area = TempData["AreaSlug"],
                        locale = TempData["LocaleSlug"],
                        category = TempData["CategorySlug"],
                        subcategory = TempData["SubCategorySlug"]
                    });
                }
            }
            return View();
        }


        [HttpGet]
        public IActionResult AdminEditPost(int? id)
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                Post post = _commonService.GetPostById(id);


                ViewBag.Users = _adminService.ListUsers(adminId);
                ViewBag.Areas = _adminService.ListAllAreas(adminId);
                ViewBag.Locales = _adminService.ListAllLocales(adminId);
                ViewBag.Categories = _adminService.ListAllCategories(adminId);
                ViewBag.Subcategories = _adminService.ListAllSubcategories(adminId);

                //Dropdown box starting indexes 
                ViewBag.UserId = post.OwnerId;
                ViewBag.AreaId = post.AreaId;
                ViewBag.CategoryId = post.CategoryId;
                ViewBag.LocaleId = post.Locale;
                ViewBag.SubcategoryId = post.SubcategoryId;



                return View(post);
            }
            return View("AccessDenied");
        }

        [HttpPost]
        public IActionResult AdminEditPost(Post model)
        {
            if (ModelState.IsValid && User.Identity.IsAuthenticated)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                if (_adminService.IsAdminValid(adminId))
                {
                    Outcome response = _adminService.ModifyPost(adminId,
                                                               model.Id,
                                                               model.Title,
                                                               model.Body,
                                                               model.OwnerId,
                                                               model.AreaId,
                                                               model.LocaleId,
                                                               model.CategoryId,
                                                               model.SubcategoryId);
                    if (response.fail)
                    {
                        ViewBag.ErrorMessage = response.Message();
                        return View("AccessDenied");
                    }
                    return RedirectToAction("KeywordSearch", "CategorySubCategory", new
                    {
                        area = TempData["AreaSlug"],
                        locale = TempData["LocaleSlug"],
                        category = TempData["CategorySlug"],
                        subcategory = TempData["SubCategorySlug"]
                    });
                }
            }
            return View();
        }

        [HttpGet]
        public IActionResult PostInformation(int id)
        {
            Post post = _commonService.GetPostById(id);
            return View(post);
        }

        [HttpGet]
        public IActionResult AllPosts()
        {
            TempData.Keep();
            if (User.Identity.IsAuthenticated)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                if (_adminService.IsAdminValid(adminId))
                {
                    return View(_adminService.ListPosts(adminId));
                }
                return RedirectToAction("KeywordSearch", "CategorySubCategory", new
                {
                    area = TempData["AreaSlug"],
                    locale = TempData["LocaleSlug"],
                    category = TempData["CategorySlug"],
                    subcategory = TempData["SubCategorySlug"]
                });
            }
            return View("AccessDenied");
        }

        [HttpGet]
        public IActionResult CreatePost()
        {
            if (User.Identity.IsAuthenticated)
            {
                string areaName;
                string categoryName;
                int areaId = -1;
                int categoryId = -1;
                if (TempData["AreaSlug"] != null )
                {
                    areaName = TempData["AreaSlug"].ToString();
                    areaName = _commonService.RemoveSlug(areaName);
                    areaId = _commonService.AreaNameToId(areaName);
                    ViewBag.AreaId = areaId;
                }
                if (TempData["CategorySlug"] != null)
                {
                    categoryName = TempData["CategorySlug"].ToString();
                    categoryName = _commonService.RemoveSlug(categoryName);
                    categoryId = _commonService.CategoryNameToId(categoryName);
                    ViewBag.CategoryId = categoryId;
                }
                ViewBag.Areas = _commonService.ListAreas();
                if (areaId > -1)
                {
                    ViewBag.Locales = _commonService.ListLocales(areaId);
                }
                ViewBag.Categories = _commonService.ListCategories();
                if (categoryId > -1)
                {
                    ViewBag.Subcategories = _commonService.ListSubcategories(categoryId);
                }

                TempData["OwnerId"] = _commonService.UserNameToId(User.Identity.Name);
                return View();
            }
            return View("AccessDenied");
        }

        [HttpPost]
        public IActionResult CreatePost(Post model)
        {
            if (ModelState.IsValid && User.Identity.IsAuthenticated)
            {
                string userId = _commonService.UserNameToId(User.Identity.Name);
                if (_adminService.IsAdminValid(userId))
                {
                    return RedirectToAction("AdminCreatePost");
                }
                Outcome response =_userService.CreatePost(model.Title, model.Body, userId, 
                    model.AreaId, model.LocaleId, model.CategoryId, model.SubcategoryId);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return RedirectToAction("MyPosts");
            }
            return View("AccessDenied");
        }

        [HttpGet]
        public IActionResult EditPost(int? id)
        {
            if (User.Identity.IsAuthenticated)
            {
                Post post = _commonService.GetPostById(id);

                string areaName;
                string categoryName;
                ViewBag.OwnerId = post.OwnerId;
                int areaId = post.AreaId;
                int categoryId = post.CategoryId;
                if (TempData["AreaSlug"] != null)
                {
                    areaName = TempData["AreaSlug"].ToString();
                    areaName = _commonService.RemoveSlug(areaName);
                    areaId = _commonService.AreaNameToId(areaName);
                    ViewBag.AreaId = areaId;
                }
                if (TempData["CategorySlug"] != null)
                {
                    categoryName = TempData["CategorySlug"].ToString();
                    categoryName = _commonService.RemoveSlug(categoryName);
                    categoryId = _commonService.CategoryNameToId(categoryName);
                    ViewBag.CategoryId = categoryId;
                }
                ViewBag.Areas = _commonService.ListAreas();
                if (areaId > -1)
                {
                    ViewBag.Locales = _commonService.ListLocales(areaId);
                }
                ViewBag.Categories = _commonService.ListCategories();
                if (categoryId > -1)
                {
                    ViewBag.Subcategories = _commonService.ListSubcategories(categoryId);
                }
                return View(post);
            }
            return View("AccessDenied");
        }

        [HttpPost]
        public IActionResult EditPost(Post model)
        {
            if (ModelState.IsValid && User.Identity.IsAuthenticated)
            {
                string userId = _commonService.UserNameToId(User.Identity.Name);
                if (_adminService.IsAdminValid(userId))
                {
                    return RedirectToAction("AdminEditPost");
                }
                Outcome response = _userService.ModifyPost(model.Id, model.Title, model.Body, userId,
                    model.AreaId, model.LocaleId, model.CategoryId, model.SubcategoryId);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return RedirectToAction("MyPosts");
            }
            return View("AccessDenied");
        }

        [HttpGet]
        public IActionResult DeletePost(int? id)
        {
            if (id == null)
            {
                return StatusCode(400);
            }
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                return View(_commonService.GetPostById(id));
            }
            else if (User.Identity.IsAuthenticated && _commonService.UserExists(adminId))
            {
                return View(_commonService.GetPostById(id));
            }
            return View("AccessDenied");
        }

        [HttpPost, ActionName("DeletePost")]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePostConfirmed(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.DeletePost(adminId, id);
                if (response.fail)
                {
                    if(_commonService.UserExists(adminId))
                    {
                        Outcome userResp = _userService.DeleteMyPost(id, adminId);
                        if (userResp.fail)
                        {
                            ViewBag.ErrorMessage = response.Message();
                            return View("AccessDenied");
                        }
                    }
                    else
                    {
                        ViewBag.ErrorMessage = response.Message();
                        return View("AccessDenied");
                    }
                }
                return RedirectToAction("MyPosts");
            }
            return View();
        }

        public IActionResult MyPosts()
        {
            if (User.Identity.IsAuthenticated)
            {
                string UserId = _commonService.UserNameToId(User.Identity.Name);
                return View(_userService.GetUnexpiredPosts(UserId, DateTime.Now));
            }
            return View("AccessDenied");
        }

        public IActionResult FillLocales(int AreaId)
        {
            return Json(_commonService.ListLocales(AreaId));
        }

        public IActionResult FillSubcategories(int CategoryId)
        {
            return Json(_commonService.ListSubcategories(CategoryId));
        }
    }
}