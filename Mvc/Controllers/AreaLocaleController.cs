﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic;
using DataModel;
using Mvc.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.WebUtilities;
using System.Net;

namespace Mvc.Controllers
{
    public class AreaLocaleController : Controller
    {
        private BusinessDbContext dataDb;
        private readonly AdminService _adminService;
        private CommonService _commonService;

        public AreaLocaleController(BusinessDbContext businessDbContext, AdminService adminService)
        {
            dataDb = businessDbContext;
            _adminService = adminService;
            _commonService = new CommonService(dataDb);
        }
        
        public IActionResult Index(string area, string locale, string category, string subcategory)
        {
            Area properArea = _commonService.GetAreaFromSlug(area);
            if (properArea == null)
            {
                return Redirect("/");
            }
            ViewBag.AreaSlug = area;
            TempData["AreaSlug"] = area;
            ViewBag.Area = properArea.Name;
            ViewBag.Locales = _commonService.ListLocales(properArea.Id);
            ViewBag.Categories = _commonService.ListCategories();
            foreach (Category cat in ViewBag.Categories)
            {
                cat.Subcategories = _commonService.ListSubcategories(cat.Id);
                // cat.Subcategories = dataDb.Subcategories.Where(sc => sc.Category == cat).ToList();
            }
            if (category != null)
            {
                TempData["CategorySlug"] = category;
                Category properCategory = _commonService.GetCategoryFromSlug(category);
                ViewBag.SubCategories = _commonService.ListSubcategories(properCategory.Id);
            }
            if (locale == null)
            {
                ViewBag.LocaleSlug = String.Empty;
                TempData["LocaleSlug"] = String.Empty;
                ViewBag.Locale = String.Empty;
            }
            else
            {
                ViewBag.LocaleSlug = locale;
                TempData["LocaleSlug"] = locale;
                ViewBag.Locale = _commonService.GetLocaleFromSlug(locale).Name;
            }
            TempData.Keep();
            return View("Views/AreaLocale/Index.cshtml");
        }

        [HttpGet]
        public IActionResult AreaCreate()
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                return View();
            }
            return View("AccessDenied");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AreaCreate(Area model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid)
            {
                Area area = new Area { Name = model.Name };
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.CreateArea(adminId, area.Name);
                if(response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return View("AreaList", _commonService.ListUnhiddenAreas());
            }
            return View();
        }

        [HttpGet]
        public IActionResult LocaleCreate()
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                ViewBag.Areas = _commonService.ListUnhiddenAreas();
                return View();
            }
            return View("AccessDenied");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult LocaleCreate(AdminUserViewModel model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.CreateLocale(adminId, model.locale.Name, model.locale.AreaId);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return RedirectToAction("LocaleList");
            }
            return View();
        }

        public IActionResult AreaList()
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                return View(_commonService.ListUnhiddenAreas());
            }
            return View("AccessDenied");
        }

        public IActionResult LocaleList()
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                return View(_adminService.ListAllLocales(adminId));
            }
            return View("AccessDenied");
        }

        public IActionResult DetailsArea(int? id)
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                if (id == null)
                {
                    return StatusCode(400);
                }
                Area area = _commonService.GetArea((int)id);
                if (area == null)
                {
                    return NotFound();
                }
                ViewBag.AreaLocales = _commonService.ListLocales((int)id);
                return View(area);
            }
            return View("AccessDenied");
        }

        //Below are the edit actions almost identical to create except in the get action a view is returned
        [HttpGet]
        public IActionResult AreaEdit(int? id)
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                return View(_commonService.GetArea((int)id));
            }
            return View("AccessDenied");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AreaEdit(Area model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.ModifyArea(adminId, model.Id, model.Name);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return View("AreaList", _commonService.ListUnhiddenAreas());

            }
            return View();
        }

        [HttpGet]
        public IActionResult LocaleEdit(int? id)
        {
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                ViewBag.Areas = _commonService.ListUnhiddenAreas();
                return View(_commonService.GetLocale((int)id));
            }
            return View("AccessDenied");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult LocaleEdit(AdminUserViewModel model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.ModifyLocale(adminId, model.locale.Id, model.locale.Name, model.locale.AreaId);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return View("LocaleList");
            }
            return View();
        }

        // Hide Area and associated Locales
        [HttpGet]
        public IActionResult AreaDelete(int? id)
        {
            if (id == null)
            {
                return StatusCode(400);
            }
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                return View(_commonService.GetArea((int)id));
            }
            return View("AccessDenied");
        }

        [HttpPost, ActionName("AreaDelete")]
        [ValidateAntiForgeryToken]
        public IActionResult AreaDeleteConfirmed(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.HideArea(adminId, id);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return View("AreaList", _commonService.ListUnhiddenAreas());
            }
            return View();
        }

        [HttpGet]
        public IActionResult LocaleDelete(int? id)
        {
            if (id == null)
            {
                return StatusCode(400);
            }
            string adminId = _commonService.UserNameToId(User.Identity.Name);
            if (User.Identity.IsAuthenticated && _adminService.IsAdminValid(adminId))
            {
                return View(_commonService.GetLocale((int)id));
            }
            return View("AccessDenied");
        }

        [HttpPost, ActionName("LocaleDelete")]
        [ValidateAntiForgeryToken]
        public IActionResult LocaleDeleteConfirmed(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("AccessDenied");
            }
            if (ModelState.IsValid)
            {
                string adminId = _commonService.UserNameToId(User.Identity.Name);
                Outcome response = _adminService.HideLocale(adminId, id);
                if (response.fail)
                {
                    ViewBag.ErrorMessage = response.Message();
                    return View("AccessDenied");
                }
                return View("LocaleList");
            }
            return View();
        }
    }
}