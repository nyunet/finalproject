﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BusinessLogic;
using DataModel;
using Mvc.Models;

namespace Mvc.Controllers
{
    public class HomeController : Controller
    {
        private BusinessDbContext dataDb;
        private CommonService cs;
        public HomeController(BusinessDbContext businessDbContext)
        {
            dataDb = businessDbContext;
            cs = new CommonService(dataDb);
        }

        public IActionResult IndexButton()
        {
            return Redirect("/");
        }

        public async Task<IActionResult> Index()
        {
            //Test for the users area and use that to bring up categories and sub categories on the front page. 
            string ip = Request.HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
            IPGeographicalLocation ipModelObj = await IPGeographicalLocation.QueryGeographicalLocationAsync(ip);
            //try
            //{
                // The following works even if CitySlug is empty
                string CitySlug = cs.AddSlug(ipModelObj.City);
                // Note: there could be multiple areas / locales with the same name (just so it won't crash)
                if (dataDb.Areas.Where(a => a.GetSlug() == CitySlug).ToList().Count > 0)
                {
                    // City exists in Areas
                    return Redirect("browse/" + CitySlug);
                }
                List<Locale> matchingLocales = dataDb.Locales.Include(locales => locales.Area).Where(l => l.GetSlug() == CitySlug).ToList();
                if (matchingLocales.Count > 0)
                {
                    // City exists in Locales
                    Locale locale = matchingLocales[0];
                    return Redirect("browse/" + locale.Area.GetSlug() + "/" + locale.GetSlug());
                }
                return View(dataDb.Areas.Include(area => area.Locales).ToList());
            //}
            //catch (Exception e)
            //{
            //    using (StreamWriter w = System.IO.File.AppendText("log.txt"))
            //    {
            //        Log(e.Message, w);
            //        Log("Inner Exception: " + e.InnerException.Message, w);
            //        Log(e.StackTrace, w);
            //    }
            //    return View();
            //}
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.WriteLine("  :");
            w.WriteLine("  :{0}", logMessage);
            w.WriteLine("-------------------------------");
        }
    }
}
