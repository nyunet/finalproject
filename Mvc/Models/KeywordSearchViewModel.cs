﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Mvc.Models
{
    public class KeywordSearchViewModel
    {
        [Required]
        public string SearchString { get; set; }
    }
}
