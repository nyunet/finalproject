﻿using System;
using DataModel;
using Mvc.Models.AccountViewModels;

namespace Mvc.Models
{
    public class AdminUserViewModel
    {
        public User user { get; set; }
        public RegisterViewModel registerViewModel { get; set; }
        public Area area { get; set; }
        public Locale locale { get; set; }
        public Category category { get; set; }
        public Subcategory subcategory { get; set; }
    }
}
