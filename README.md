# Final Project

## Technologies used

- .NET Core
- SQL database
- MVC with scaffolding

## Extras

- IP geolocation
- XUnit testing
	- Almost full coverage of business logic
	- Using In-Memory database
- Search and filter are done only once Area and Locale are selected for performance reasons
    
## Structure

- Data model project
	- Post
	- Area
	- Locale
	- Category
	- Subcategory
	- User
	- Messages
	- IPGeographicalLocation
- Business Logic project
	- Admin service (for admin features)
	- Common service
	- User service
- XUnit testing project
	- Testing all services in the business logic project
- MVC
	- Using scaffolded login with business logic
	- Each controller has its own set of views
	- The Model is taken from the Data Model project
	- Controllers change the database using dependency injection (see StartUp.cs)
    
## How to use locally

1. Download the project as a zip file and extract or use git to clone it with:

   ```
   git clone git@bitbucket.org:nyunet/finalproject.git
   ```

1. Open the **finalproject.sln** file with Visual Studio Enterprise
1. At the top, select the **MVC** project, select **Mvc** instead of **IIs Express** and click on the Play button
1. In the browser, click on **Register**. The first account is always of type administrator.
1. Please log out and log back in to access the Administrator features

## Azure access

[http://mvc20180507010231.azurewebsites.net](http://mvc20180507010231.azurewebsites.net/)

(To be updated this evening - 05.18.2018)