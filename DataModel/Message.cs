﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System;

namespace DataModel
{
    public class Message
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [DisplayName("Message")]
        [MinLength(10, ErrorMessage = "The Response must be at least 10 characters")]
        [MaxLength(600, ErrorMessage = "The Response must be less than 600 characters")]
        public string Response { get; set; }

        public DateTime Timestamp { get; set; } // automatic (at object creation)

        [DisplayName("From")]
        [ForeignKey("Sender")]
        public string SenderId { get; set; }
        
        [DisplayName("To")]
        [ForeignKey("Receiver")]
        public string ReceiverId { get; set; }

        [ForeignKey("Post")]
        public int PostId { get; set; }

        [DisplayName("From")]
        public virtual User Sender { get; set; }

        [DisplayName("To")]
        public virtual User Receiver { get; set; }
        public virtual Post Post { get; set; }

        public Message()
        {
            Timestamp = DateTime.Now;
        }
    }
}
