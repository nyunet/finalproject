﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
    public class Post
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; } // automatic

        public DateTime Timestamp { get; set; } // automatic (at object creation)

        public DateTime Expiration { get; set; } // automatic (at object creation)

        [Required]
        [MinLength(3, ErrorMessage = "The title must be at least 3 characters")]
        [MaxLength(30, ErrorMessage = "The title must be less than 30 characters")]
        public string Title { get; set; }

        [Required]
        [DisplayName("Description")]
        [MinLength(10, ErrorMessage = "The Description must be at least 10 characters")]
        [MaxLength(600, ErrorMessage = "The Description must be less than 600 characters")]
        public string Body { get; set; }

        [Required]
        [DisplayName("Owner")]
        [ForeignKey("Owner")]
        public string OwnerId { get; set; } // automatic (from authentication)

        [Required]
        [DisplayName("Area")]
        [ForeignKey("Area")]
        public int AreaId { get; set; }

        [Required]
        [DisplayName("Locale")]
        [ForeignKey("Locale")]
        public int LocaleId { get; set; }

        [Required]
        [DisplayName("Category")]
        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        [Required]
        [DisplayName("Subcategory")]
        [ForeignKey("Subcategory")]
        public int SubcategoryId { get; set; }

        public bool Active { get; set; }

        public Post()
        {
            Timestamp = DateTime.Now;
            Expiration = Timestamp.AddDays(5);
            Active = true;
        }

        public virtual User Owner { get; set; }
        public virtual Area Area { get; set; }
        public virtual Locale Locale { get; set; }
        public virtual Category Category { get; set; }
        public virtual Subcategory Subcategory { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
    }
}
