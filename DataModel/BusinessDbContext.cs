﻿using Microsoft.EntityFrameworkCore;

namespace DataModel
{
    public class BusinessDbContext : DbContext
    {
        //public static IConfiguration Configuration { get; set; }
        public BusinessDbContext() { } // maybe remove
        public BusinessDbContext(DbContextOptions<BusinessDbContext> options) : base(options) { }


        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Subcategory> Subcategories { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<Locale> Locales { get; set; }
        public DbSet<IPGeographicalLocation> IPGeographicalLocations { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server = (localdb)\\mssqllocaldb; Database = aspnet - Mvc - 3E9D9C42 - 75C0 - 4265 - B971 - 1CAFC5A512AB; Trusted_Connection = True; MultipleActiveResultSets = true");
        //}
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<Message>()
            .HasOne(x => x.Sender)
            .WithMany(x => x.SentMessages)
            .IsRequired(false)
            .HasForeignKey(x => x.SenderId)
            .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Message>()
            .HasOne(x => x.Receiver)
            .WithMany(x => x.ReceivedMessages)
            .IsRequired(false)
            .HasForeignKey(x => x.ReceiverId)
            .OnDelete(DeleteBehavior.SetNull);
        }

    }
}
