﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
    public class Subcategory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        public bool Hidden { get; set; }

        public virtual Category Category { get; set; }

        public string GetSlug()
        {
            return Name.ToLower().Replace(" ", "-");
        }

        public Subcategory()
        {
            Hidden = false;
        }
    }
}
