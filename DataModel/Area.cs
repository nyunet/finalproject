﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
    public class Area
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public bool Hidden { get; set; }

        public virtual ICollection<Locale> Locales { get; set; }

        public string GetSlug()
        {
            return Name.ToLower().Replace(" ", "-");
        }

        public Area()
        {
            Hidden = false;
        }
    }
}
