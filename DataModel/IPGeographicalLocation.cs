﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class IPGeographicalLocation
    {
        private static string AccessKey = "4c02d985da5999d45e0c654b71470380";
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [JsonProperty("ip")]
        public string IP { get; set; }

        [JsonProperty("country_code")]

        public string CountryCode { get; set; }

        [JsonProperty("country_name")]

        public string CountryName { get; set; }

        [JsonProperty("region_code")]

        public string RegionCode { get; set; }

        [JsonProperty("region_name")]

        public string RegionName { get; set; }

        [JsonProperty("city")]

        public string City { get; set; }

        [JsonProperty("zip_code")]

        public string ZipCode { get; set; }

        [JsonProperty("time_zone")]

        public string TimeZone { get; set; }

        [JsonProperty("latitude")]

        public float? Latitude { get; set; }

        [JsonProperty("longitude")]

        public float? Longitude { get; set; }

        [JsonProperty("metro_code")]

        public int? MetroCode { get; set; }

        private IPGeographicalLocation() { }

        public static async Task<IPGeographicalLocation> QueryGeographicalLocationAsync(string ipAddress)
        {
            HttpClient client = new HttpClient();
            string result = await client.GetStringAsync("http://api.ipstack.com/" + ipAddress + "?access_key=" + AccessKey + "&output=json&legacy=1");
                //http://freegeoip.net/json/" + ipAddress);
            var settings = new JsonSerializerSettings() { ContractResolver = new NullToEmptyStringResolver() };

            return JsonConvert.DeserializeObject<IPGeographicalLocation>(result, settings);
        }
    }

    public class NullToEmptyStringResolver : Newtonsoft.Json.Serialization.DefaultContractResolver
    {
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            return type.GetProperties()
                    .Select(p => {
                        var jp = base.CreateProperty(p, memberSerialization);
                        jp.ValueProvider = new NullToEmptyStringValueProvider(p);
                        return jp;
                    }).ToList();
        }
    }

    public class NullToEmptyStringValueProvider : IValueProvider
    {
        PropertyInfo _MemberInfo;
        public NullToEmptyStringValueProvider(PropertyInfo memberInfo)
        {
            _MemberInfo = memberInfo;
        }

        public object GetValue(object target)
        {
            object result = _MemberInfo.GetValue(target);
            if (_MemberInfo.PropertyType == typeof(string) && result == null) result = "";
            return result;

        }

        public void SetValue(object target, object value)
        {
            if (_MemberInfo.PropertyType == typeof(string) && value == null) value = "";
            if (_MemberInfo.PropertyType == typeof(Single?) && value == null) value = Convert.ToSingle(0);
            _MemberInfo.SetValue(target, value);
        }
    }
}
