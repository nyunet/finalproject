﻿using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
    public class User
    {
        public string Id { get; set; } // obtained from Built-in login from MVC

        [Required]
        public string Type { get; set; }
        // "user" or "admin"

        [Required]
        [DisplayName("username")]
        [MinLength(3, ErrorMessage = "The username must be at least 3 characters")]
        [MaxLength(30, ErrorMessage = "The username must be less than 30 characters")]
        public string Name { get; set; }

        [Required]
        public bool Active { get; set; }

        public User()
        {
            Active = true;
        }

        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Message> SentMessages { get; set; }
        public virtual ICollection<Message> ReceivedMessages { get; set; }
    }
}
