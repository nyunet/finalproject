﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
    public class Locale
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public bool Hidden { get; set; }

        [ForeignKey("Area")]
        public int AreaId { get; set; }

        public virtual Area Area { get; set; }

        public string GetSlug()
        {
            return Name.ToLower().Replace(" ", "-");
        }

        public Locale()
        {
            Hidden = false;
        }
    }
}
